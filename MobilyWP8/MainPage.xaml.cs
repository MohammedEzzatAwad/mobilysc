﻿namespace MobilyWP8
{
    using Microsoft.Phone.Controls;

    using MobilyWP8.ViewModels;

    public partial class MainView : PhoneApplicationPage
    {
        public MainView()
        {
            this.InitializeComponent();
        }

        private void LogInButtonClick(object sender, System.EventArgs e)
        {
            var dataContext = (MainViewModel)DataContext;
            dataContext.NavigateToLogIn();
        }

       
    }
}