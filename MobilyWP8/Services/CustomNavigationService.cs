﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Input;
using MobilyWP8.Views;
using System.Windows.Navigation;

namespace MobilyWP8.Services
{
    public class CustomNavigationService : ICustomNavigationService
    {
        /// <summary>
        /// Go to login page
        /// </summary>
        public void GoToLoginPage()
        {
            //new NavigationService().Navigate(typeof(LoginView));
            App.RootFrame.Navigate(new Uri("/Views/LoginView.Xaml", UriKind.Relative));
        }

        /// <summary>
        /// Go to main page
        /// </summary>
        public void GoToMainPage()
        {
            App.RootFrame.Navigate(new Uri("/MainPage.xaml", UriKind.Relative));
        }

        public void GoToProductAndServices()
        {
            App.RootFrame.Navigate(new Uri(string.Format("/Views/ProductAndServicesView.Xaml"), UriKind.Relative));
        }


        public void GoToAppSettingsView()
        {
            App.RootFrame.Navigate(new Uri("/Views/AppSettingsView.xaml", UriKind.Relative));
        }

        public void GoToNewsView()
        {
            App.RootFrame.Navigate(new Uri("/Views/NewsView.xaml", UriKind.Relative));
        }

        public void GoToPromotionsDetailsView()
        {
            App.RootFrame.Navigate(new Uri("/Views/PromotionsDetailsView.xaml", UriKind.Relative));
        }

        public void GoToMobilyNewsDetailsView()
        {
            App.RootFrame.Navigate(new Uri("/Views/MobilyNewsDetailsView.xaml", UriKind.Relative));
        }

        public void GoToAlhawaNewsDetailsView()
        {
            App.RootFrame.Navigate(new Uri("/Views/AlhawaNewsDetailsView.xaml", UriKind.Relative));
        }

        public void GoToMyAccountView()
        {
            App.RootFrame.Navigate(new Uri("/Views/MyAccountView.xaml", UriKind.Relative));
        }

        public void GoToProductDetailsView()
        {
            App.RootFrame.Navigate(new Uri("/Views/ProductDetailsView.xaml", UriKind.Relative));
        }

        public void GoToBillingMethodView()
        {
            App.RootFrame.Navigate(new Uri("/Views/BillingMethodView.xaml", UriKind.Relative));
        }
        public void GoToEditServicesView()
        {
            App.RootFrame.Navigate(new Uri("/Views/EditServicesView.xaml", UriKind.Relative));
        }
    }
}
