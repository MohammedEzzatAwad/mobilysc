﻿namespace MobilyWP8.Services
{  
    public interface ICustomNavigationService
    {
        /// <summary>
        /// Navigates to the login page 
        /// </summary>
        void GoToLoginPage();

        /// <summary>
        /// Navigates to the MainPage 
        /// </summary>
        void GoToMainPage();

        /// <summary>
        /// Navigates to the ProductandServicesView
        /// </summary>
        void GoToProductAndServices();

        /// <summary>
        /// Navigate to app SettingView
        /// </summary>
        void GoToAppSettingsView();

        /// <summary>
        /// Navigates to NewsView
        /// </summary>
        void GoToNewsView();

        /// <summary>
        /// Navigates to PromotionsDetailsView
        /// </summary>
        void GoToPromotionsDetailsView();

        /// <summary>
        /// Navigates to MobilyNewsDetailsView
        /// </summary>
        void GoToMobilyNewsDetailsView();

        /// <summary>
        /// Navigates to AlhawaNewsDetailsView
        /// </summary>
        void GoToAlhawaNewsDetailsView();

        /// <summary>
        /// Navigates to MyAccountView
        /// </summary>
        void GoToMyAccountView();

        /// <summary>
        /// Navigates to product details view
        /// </summary>
        void GoToProductDetailsView();

        /// <summary>
        /// Navigates to BillingMethod view
        /// </summary>
        void GoToBillingMethodView();
        
        /// Navigates tp edit services view 
        /// </summary>
        void GoToEditServicesView();

    }
}
