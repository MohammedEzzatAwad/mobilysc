﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;

namespace MobilyWP8.Converters
{
    public class LanguageTemplateSelector : DataTemplateSelector
    {
        public DataTemplate ArabicTemplate { get; set; }

        public DataTemplate EnglishTemplate { get; set; }

        public override DataTemplate SelectTemplate(object item, DependencyObject container)
        {
            var currentCulture = Thread.CurrentThread.CurrentCulture;
            return currentCulture.Name == "ar-SA" ? ArabicTemplate : EnglishTemplate;
        }
    }
}
