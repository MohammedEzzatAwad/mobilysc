﻿using System;
using System.Globalization;
using System.Windows.Data;
using MobilyWP8.Common;

namespace MobilyWP8.Converters
{
    public class IsLoginConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return ((value as bool?) ?? true) ? Constants.login_LOGOUT.ToLower() : Constants.login_LOGIN.ToLower();
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
