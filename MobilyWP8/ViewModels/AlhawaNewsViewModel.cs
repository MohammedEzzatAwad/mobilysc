﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Xml.Linq;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using MobilyWP8.Common;
using MobilyWP8.Resources;
using MobilyWP8.Services;
using MobilyWP8.VOs;
using System.Net;

namespace MobilyWP8.ViewModels
{
    public class AlhawaNewsViewModel : BaseViewModel
    {
        private readonly IContentChannelHelper _contentChannelHelper;

        public ContentChannel.ContentData SelectedAlhawaNews
        {
            get { return App.SelectedAlhawaNews; }
        }

        public ContentChannel.ContentData.Content ContentData { get; set; }

        public string Title { get; set; }

        public ICommand GetAlhawaNewsDataCommand { get; set; }

        public AlhawaNewsViewModel(IXmlMappers xmlMapper, ICustomNavigationService navigationService, IContentChannelHelper contentChannelHelper) : base(xmlMapper, navigationService)
        {
            _contentChannelHelper = contentChannelHelper;
            //Register Commands
            GetAlhawaNewsDataCommand = new RelayCommand(GetAlhawaNewsData);
        }

        public void GetAlhawaNewsData()
        {
            if(SelectedAlhawaNews != null)
            {
                Progress = 0;
                ProgressMessage = AppResources.LoadingText;
                IsLoading = true;

                //Get Alhawa News Data
                var webClient = new WebClient();
                webClient.DownloadStringAsync(new Uri(SelectedAlhawaNews.Link));
                webClient.Headers[HttpRequestHeader.ContentType] = "application/x-www-form-urlencoded";
                webClient.Headers[HttpRequestHeader.ContentLength] = string.Empty;
                webClient.DownloadStringCompleted += WebClientDownloadStringCompleted;
                webClient.DownloadProgressChanged += WebClientDownloadProgressChanged;
            }
        }

        void WebClientDownloadStringCompleted(object sender, DownloadStringCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                GetContentChannelData(e.Result);
                IsLoading = false;
            }
        }

        private void WebClientDownloadProgressChanged(object sender, DownloadProgressChangedEventArgs e)
        {
            Progress++;
            Debug.WriteLine(string.Format("Progress: {0} ", e.ProgressPercentage));
        }

        private void GetContentChannelData(string msg)
        {
            ContentData = _contentChannelHelper.GetContentChannelData(msg);
            ContentData.CategoryName = SelectedAlhawaNews.CategoryName;

            if (SelectedAlhawaNews.ContentType.Equals("1"))
            {
                Title = AppResources.NewsFrom3alhawaTitle;
            }
            else if (SelectedAlhawaNews.ContentType.Equals("2"))
            {
                Title = AppResources.SportsFrom3alhawaTitle;
            }
        }
    }
}
