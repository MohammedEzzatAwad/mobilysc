﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;
using GalaSoft.MvvmLight.Command;
using MobilyWP8.Common;
using MobilyWP8.Resources;
using MobilyWP8.Services;
using MobilyWP8.VOs;
using GalaSoft.MvvmLight.Messaging;

namespace MobilyWP8.ViewModels
{
    public class LatestNewsViewModel : BaseViewModel
    {
        public bool IsArabicTemplate { get; set; }
        public bool IsEnglishTemplate { get; set; }
        public NewsPromotionVO SelectedPromotionsNews
        {
            get { return App.SelectedPromotionsNews; }
        }
        public NewsPromotionVO SelectedMobilyNews
        {
            get { return App.SelectedMobilyNews; }
        }

        public ICommand GetLatestNewsDataCommand { get; set; }

        public LatestNewsViewModel(IXmlMappers xmlMapper, ICustomNavigationService navigationService) : base(xmlMapper, navigationService)
        {
            //Register Commands
            GetLatestNewsDataCommand = new RelayCommand(GetLatestNewsData);
        }

        private void GetLatestNewsData()
        {
            var currentCulture = Thread.CurrentThread.CurrentCulture;
            switch (currentCulture.Name)
            {
                case "ar-SA":
                    IsArabicTemplate = true;
                    IsEnglishTemplate = false;
                    break;
                case "en-US":
                    IsArabicTemplate = false;
                    IsEnglishTemplate = true;
                    break;
            }
        }
    }
}
