﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO.IsolatedStorage;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using MobilyWP8.Common;
using MobilyWP8.Services;
using MobilyWP8.VOs;
using MobilyWP8.Resources;

namespace MobilyWP8.ViewModels
{
    public class BaseViewModel : ViewModelBase
    {
        protected readonly IXmlMappers XmlMapper;
        protected readonly AppSettings Settings;
        protected readonly ICustomNavigationService NavigationService;
        
        private bool _isLoading;
        public bool IsLoading
        {
            get { return _isLoading; }
            set { Set("IsLoading", ref _isLoading, value); }
        }

        private int _progress;
        public int Progress
        {
            get { return _progress; }
            set { Set("Progress", ref _progress, value); }
        }

        private string _progressMessage;
        public string ProgressMessage
        {
            get { return _progressMessage; }
            set { Set("ProgressMessage", ref _progressMessage, value); }
        }

        public BaseViewModel(IXmlMappers xmlMapper, ICustomNavigationService navigationService)
        {
            XmlMapper = xmlMapper;
            Settings = new AppSettings();
            NavigationService = navigationService;

            NavigateToLogCommand = new RelayCommand( NavigateToLog);
            NavigateToAppSettingsCommand = new RelayCommand(NavigateToAppSettings);

        }
        public ICommand NavigateToLogCommand { get; set; }

        public void NavigateToLog()
        {
            if (Settings.IsLoginSetting)
            {
                Logout();
                
            }
            else
            {
                NavigationService.GoToLoginPage();
            }
        }

        public ICommand NavigateToAppSettingsCommand { get; set; }


        public void NavigateToAppSettings()
        {
            NavigationService.GoToAppSettingsView();
        }

        public void Logout()
        {
            string xmlForLogout = XmlMapper.CreateXmlForLogout(App.Person.HashCode);
            WebClient webClient = new WebClient();
            webClient.Headers[HttpRequestHeader.ContentType] = "application/x-www-form-urlencoded";

            webClient.Headers[HttpRequestHeader.ContentLength] = Convert.ToString(xmlForLogout.Length);
            webClient.UploadStringCompleted += WebClientUploadStringCompleted;
            webClient.UploadStringAsync(new Uri(Constants.request_url), "POST", xmlForLogout);
        }

        private void WebClientUploadStringCompleted(object sender, UploadStringCompletedEventArgs e)
        {
            Debug.WriteLine(e.Result);
            if (e.Error == null)
            {
                var error = XmlMapper.GetError(e.Result);
                if (!string.IsNullOrEmpty(error))
                {
                    MessageBox.Show(error);
                    return;
                }

                //set IsLogin to false
                Settings.AddOrUpdateValue(Settings.IsLoginSettingKeyName, false);
                NavigationService.GoToMainPage();
            }
        }

        protected void UpdateUiFrom(string responseMessage, MessageTypeEnum messageType)
        {
            var error = XmlMapper.GetError(responseMessage);
            if (!string.IsNullOrEmpty(error))
            {
                MessageBox.Show(error, AppResources.Error, MessageBoxButton.OK);
                return;
            }
            switch (messageType)
            {
                case MessageTypeEnum.BillInfoMessage:
                    App.Person.BillInfo.ParseXmlBillInfo(responseMessage);
                    break;
                case MessageTypeEnum.BillPersonalInfoMessage:
                    App.Person.BillPersonalInfo.ParseXmlBillPersonalInfo(responseMessage);
                    break;
                case MessageTypeEnum.BillSummaryMessage:
                    App.Person.BillSummary.ParseXmlBillSummary(responseMessage);
                    break;
                case MessageTypeEnum.BalanceMessage:
                    App.Person.Balance.ParseXml(responseMessage);
                    break;
                case MessageTypeEnum.NeqatyMessage:
                    App.Person.Neqaty.ParseXml(responseMessage);
                    break;
                case MessageTypeEnum.LatestNewsMessage:
                    App.Person.NotificationAndServices.ParseXmlNews(responseMessage);
                    break;
                case MessageTypeEnum.PromotionsMessage:
                    App.Person.NotificationAndServices.ParseXmlPromotions(responseMessage);
                    break;
                case MessageTypeEnum.BillMethod:
                    App.Person.BillMethod.ParseBillMethodXml(responseMessage);
                    break;
                case MessageTypeEnum.SupplementaryServiceMessage:
                    App.Person.SupplementaryServices.ParseXml(responseMessage);
                    break;
            }
        }
    }
}
