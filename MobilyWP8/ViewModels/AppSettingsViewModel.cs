﻿using System.Threading;
using System.Windows.Input;
using GalaSoft.MvvmLight.Command;
using MobilyWP8.Common;
using MobilyWP8.Resources;
using MobilyWP8.Services;

namespace MobilyWP8.ViewModels
{
    public class AppSettingsViewModel : BaseViewModel
    {
        private int _languageIndex;
        private const string LanguageIndexPropertyName = "LanguageIndex";
        private readonly ICustomNavigationService _customNavigationService;
        public int LanguageIndex
        {
            get { return _languageIndex; }
            set { Set(LanguageIndexPropertyName, ref _languageIndex, value); }
        }
        public AppSettingsViewModel(IXmlMappers xmlMapper, ICustomNavigationService navigationService)
            : base(xmlMapper, navigationService)
        {
            _customNavigationService = navigationService;
            ApplySettingsCommand = new RelayCommand(ApplySettings); 
        }

        public ICommand ApplySettingsCommand { get; set; }

        public void ApplySettings()
        {
            //set language
            if (_languageIndex == 0)
            {
                Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en-US");
                Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo("en-US");
                ((AppResourcesManagerDynamic)App.Current.Resources["AppResourcesManagerDynamic"]).ResetResources();
            }
            else
            {
                Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("ar-SA");
                Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo("ar-SA");
                ((AppResourcesManagerDynamic)App.Current.Resources["AppResourcesManagerDynamic"]).ResetResources();
            }
            _customNavigationService.GoToMainPage();
        }
    }
}