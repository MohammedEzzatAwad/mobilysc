﻿using System;
using System.Diagnostics;
using System.Globalization;
using System.Net;
using System.Windows;
using System.Windows.Input;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using MobilyWP8.Common;
using MobilyWP8.Services;
using MobilyWP8.VOs;
using MobilyWP8.Resources;

namespace MobilyWP8.ViewModels
{
    public class LoginViewModel : BaseViewModel //ViewModelBase
    {
        
        private readonly ICustomNavigationService _customNavigationService;
        private readonly IXmlMappers _xmlMapper;
        private readonly AppSettings _settings;
        private string _password;
        private string _username;

        public LoginViewModel(IXmlMappers xmlMapper, ICustomNavigationService customNavigationService) : base(xmlMapper, customNavigationService)
        {
            _xmlMapper = xmlMapper;
            _customNavigationService = customNavigationService;

            _settings = new AppSettings();
            
            //Register Commands
            LoginCommand = new RelayCommand(Login);
            
            Username = "qatestraj";
            Password = "ePortal2012";
        }
        
        public string Username
        {
            get { return _username; }
            set { Set(Constants.login_UserName, ref _username, value); }
        }

        public string Password
        {
            get { return _password; }
            set { Set(Constants.login_Password, ref _password, value); }
        }

        public ICommand LoginCommand { get; set; }


        private bool _isLOggingIn;

        public bool IsLoggingIn
        {
            get { return _isLOggingIn; }
            set { Set("IsLoggingIn", ref _isLOggingIn, value); }
        }

        private int _progress;

        public int Progress
        {
            get { return _progress; }
            set {  Set("Progress", ref _progress, value); }
        }

        private string _progressName;

        public string ProgressName
        {
            get { return _progressName; }
            set { Set("ProgressName", ref _progressName, value); }
        }
        
        public async void Login()
        {
            Progress = 0;

            IsLoggingIn = true;

           string xmlForLogin = _xmlMapper.CreateXmlForLogin(Username, Password);

            var webClient = App.Client;
            webClient.Headers[HttpRequestHeader.ContentType] = "application/x-www-form-urlencoded";


            webClient.Headers[HttpRequestHeader.ContentLength] = Convert.ToString(xmlForLogin.Length);
            webClient.UploadStringCompleted += webClient_UploadStringCompleted;
            webClient.UploadProgressChanged += webClient_UploadProgressChanged;
            webClient.UploadStringAsync(new Uri(Constants.request_url), "POST", xmlForLogin);
            ProgressName = "Loading User data";
            Progress++;
        }

        private void webClient_UploadStringCompleted(object sender, UploadStringCompletedEventArgs e)
        {
            Debug.WriteLine(e.Result);
            //var response = e.;
            if (e.Error == null)
            {
                var error = _xmlMapper.GetError(e.Result);
                if (!string.IsNullOrEmpty(error))
                {
                    MessageBox.Show(AppResources.YouAreNotLoggedInText);
                    return;
                }
                //Dissect HashCode from Login Response Message
                App.Person.HashCode = _xmlMapper.GetHashCode(e.Result);
                if (App.Person.HashCode == null && App.Person.HashCode == string.Empty)
                    return;
                string content = _xmlMapper.CreateXmlForBalanceBillNeqaty(App.Person.HashCode,
                                                                          Constants.balancebill_GET_BILL_INFO);
                var webClientGetBilling = new WebClient();

                webClientGetBilling.Headers[HttpRequestHeader.ContentLength] = content.Length.ToString();
                webClientGetBilling.UploadStringCompleted += webClientGetBilling_UploadStringCompleted;
                webClientGetBilling.UploadProgressChanged += webClient_UploadProgressChanged;
                webClientGetBilling.UploadStringAsync(new Uri(Constants.request_url), "POST", content);

                ProgressName = "Loading Billing data";
                Progress++;
            }
        }

        private void webClientGetBilling_UploadStringCompleted(object sender, UploadStringCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                UpdateUiFrom(e.Result, MessageTypeEnum.BillInfoMessage);
            }
            string content = _xmlMapper.
                CreateXmlForBalanceBillNeqaty(App.Person.HashCode,
                                              Constants.balancebill_GET_BILL_PERSONAL_INFO);
            var webClientGetPersonalInfo = new WebClient();
            webClientGetPersonalInfo.Headers[HttpRequestHeader.ContentLength] = content.Length.ToString();
            webClientGetPersonalInfo.UploadStringCompleted += webClientGetPersonalInfo_UploadStringCompleted;
            webClientGetPersonalInfo.UploadProgressChanged += webClient_UploadProgressChanged;
            webClientGetPersonalInfo.UploadStringAsync(new Uri(Constants.request_url), "POST", content);

            ProgressName = "Loading personal info";
            Progress++;

            
        }

        void webClientGetPersonalInfo_UploadStringCompleted(object sender, UploadStringCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                UpdateUiFrom(e.Result, MessageTypeEnum.BillPersonalInfoMessage);
            }

            var content = _xmlMapper.CreateXmlForBalanceBillNeqaty(App.Person.HashCode,
                                                                               Constants.balancebill_GET_BILL_SUMMARY);
            var webClientBillSummary = new WebClient();
            webClientBillSummary.Headers[HttpRequestHeader.ContentLength] = content.Length.ToString();
            webClientBillSummary.UploadStringCompleted += webClientBillSummary_UploadStringCompleted;
            webClientBillSummary.UploadProgressChanged += webClient_UploadProgressChanged;
            webClientBillSummary.UploadStringAsync(new Uri(Constants.request_url), "POST", content);
            ProgressName = "Loading bill summary";
            Progress++;
        }

        void webClientBillSummary_UploadStringCompleted(object sender, UploadStringCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                UpdateUiFrom(e.Result, MessageTypeEnum.BillSummaryMessage);
            }
            string content = _xmlMapper.
               CreateXmlForBalanceBillNeqaty(App.Person.HashCode,
                                             Constants.balancebill_GET_BALANCE);
            var webClientGetBalance = new WebClient();
            webClientGetBalance.Headers[HttpRequestHeader.ContentLength] = content.Length.ToString();
            webClientGetBalance.UploadStringCompleted += webClientGetBalance_UploadStringCompleted;
            webClientGetBalance.UploadProgressChanged += webClient_UploadProgressChanged;
            webClientGetBalance.UploadStringAsync(new Uri(Constants.request_url), "POST", content);

            ProgressName = "Loading balance data";
            Progress++;
        }

        void webClientGetBalance_UploadStringCompleted(object sender, UploadStringCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                UpdateUiFrom(e.Result, MessageTypeEnum.BalanceMessage);
            }

            string content = _xmlMapper.
               CreateXmlForBalanceBillNeqaty(App.Person.HashCode,
                                             Constants.neqaty_LOYALTY_VIEW_SUMMARY);
            var webClientGetNeqaty = new WebClient();
            webClientGetNeqaty.Headers[HttpRequestHeader.ContentLength] = content.Length.ToString();
            webClientGetNeqaty.UploadStringCompleted += webClientGetNeqaty_UploadStringCompleted;
            webClientGetNeqaty.UploadProgressChanged += webClient_UploadProgressChanged;
            webClientGetNeqaty.UploadStringAsync(new Uri(Constants.request_url), "POST", content);

            ProgressName = "Loading Neqaty data";
            Progress++;
        }

        void webClientGetNeqaty_UploadStringCompleted(object sender, UploadStringCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                UpdateUiFrom(e.Result, MessageTypeEnum.NeqatyMessage);
            }

            var content =  _xmlMapper.CreateXmlForNewsPromotion(App.Person.HashCode,
                                                                               Constants.GET_LATEST_NEWS_PROMOTION, "1");
            var webClientLatestNews = new WebClient();
            webClientLatestNews.Headers[HttpRequestHeader.ContentLength] = content.Length.ToString();
            webClientLatestNews.UploadStringCompleted += webClientLatestNews_UploadStringCompleted;
            webClientLatestNews.UploadProgressChanged += webClient_UploadProgressChanged;
            webClientLatestNews.UploadStringAsync(new Uri(Constants.request_url), "POST", content);
            ProgressName = "Loading latest news";
            Progress++;
        }

        void webClientLatestNews_UploadStringCompleted(object sender, UploadStringCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                UpdateUiFrom(e.Result, MessageTypeEnum.LatestNewsMessage);
            }
            var content =
                        _xmlMapper.CreateXmlForNewsPromotion(App.Person.HashCode,
                                                                               Constants.GET_LATEST_NEWS_PROMOTION, "2");
            var webClientPromotions = new WebClient();
            webClientPromotions.Headers[HttpRequestHeader.ContentLength] = content.Length.ToString();
            webClientPromotions.UploadStringCompleted += webClientPromotions_UploadStringCompleted;
            webClientPromotions.UploadProgressChanged += webClient_UploadProgressChanged;
            webClientPromotions.UploadStringAsync(new Uri(Constants.request_url), "POST", content);
            ProgressName = "Loading Promotions";
            Progress++;
        }

        void webClientPromotions_UploadStringCompleted(object sender, UploadStringCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                UpdateUiFrom(e.Result, MessageTypeEnum.PromotionsMessage);
            }
            var content = _xmlMapper.CreateXmlForBalanceBillNeqaty(App.Person.HashCode,
                                                                                         Constants.
                                                                                             BILL_METHOD_FunctionId);
            var webClientBillMethod = new WebClient();
            webClientBillMethod.Headers[HttpRequestHeader.ContentLength] = content.Length.ToString();
            webClientBillMethod.UploadStringCompleted += webClientBillMethod_UploadStringCompleted;
            webClientBillMethod.UploadProgressChanged += webClient_UploadProgressChanged;
            webClientBillMethod.UploadStringAsync(new Uri(Constants.request_url), "POST", content);
            ProgressName = "Loading BillMethod";
            Progress++;
        }

        void webClientBillMethod_UploadStringCompleted(object sender, UploadStringCompletedEventArgs e)
        {
            ProgressName = "Loading suppelments data";
            Progress++;
            if (e.Error == null)
            {
                UpdateUiFrom(e.Result, MessageTypeEnum.BillMethod);
            }

            


            var content = _xmlMapper.CreateXmlForBalanceBillNeqaty(App.Person.HashCode, Constants.SUPPLEMENTARY_SERVICES_INQUIRY);
            var webClientSupplementary = new WebClient();
            webClientSupplementary.Headers[HttpRequestHeader.ContentLength] = content.Length.ToString();
            webClientSupplementary.UploadStringCompleted += webClientSupplementary_UploadStringCompleted;
            webClientSupplementary.UploadProgressChanged += webClient_UploadProgressChanged;
            webClientSupplementary.UploadStringAsync(new Uri(Constants.request_url), "POST", content);
            

        }

        void webClientSupplementary_UploadStringCompleted(object sender, UploadStringCompletedEventArgs e)
        {
            Progress++;
            ProgressName = "Done";
            if (e.Error == null)
            {
                UpdateUiFrom(e.Result, MessageTypeEnum.SupplementaryServiceMessage);
                
                //set IsLogin to true
                _settings.AddOrUpdateValue(_settings.IsLoginSettingKeyName, true);
                _customNavigationService.GoToMainPage();
            }
            IsLoggingIn = false;
           
        }

        private void webClient_UploadProgressChanged(object sender, UploadProgressChangedEventArgs e)
        {
            Debug.WriteLine(string.Format("Progress: {0} ", e.ProgressPercentage));
        }
    }
}