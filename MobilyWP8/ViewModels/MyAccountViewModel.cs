﻿using System;
using System.Diagnostics;
using System.Net;
using System.Windows;
using System.Windows.Input;
using GalaSoft.MvvmLight.Command;
using MobilyWP8.Common;
using MobilyWP8.Services;
using MobilyWP8.VOs;

namespace MobilyWP8.ViewModels
{
    public class MyAccountViewModel : BaseViewModel
    {
        private readonly ICustomNavigationService _customNavigationService;

        public MyAccountViewModel(IXmlMappers xmlMapper, ICustomNavigationService customNavigationService) : base(xmlMapper, customNavigationService)
        {
            _customNavigationService = customNavigationService;

            SubmitTransferCommand = new RelayCommand(SubmitTransfer);
            LoadedCommand = new RelayCommand(Load);
            NavigateToBillingMethodViewCommand = new RelayCommand(NavigateToBillingMethodView);
            NavigateToEditServicesCommend = new RelayCommand(NavigateToEditSetvices);
        }

        private const string FiveAmountCheckedPropertyName = "FiveAmountChecked";
        private const string TenAmountCheckedPropertyName = "tenAmountChecked";
        private const string FifteenAmountCheckedPropertyName = "fifteenAmountChecked";
        private const string TweentyAmountCheckedPropertyName = "tweentyAmountChecked";
        private const string IsLoadingPropertyName = "IsLoading";
        private const string PhoneNumberPropertyName = "PhoneNumber";
        private const string ProgressIndexPropertyName = "ProgressIndex";
        private bool _fifteenAmountChecked;
        private bool _fiveAmountChecked;
        private bool _isLoading;
        private string _phoneNumber;
        private int _progressIndex;
        private bool _tenAmountChecked;
        private bool _tweentyAmountChecked;

        

        public int Index
        {
            get { return App.Index; }
        }

        public ICommand LoadedCommand { get; set; }

        public ICommand SubmitTransferCommand { get; set; }
        
        public ICommand NavigateToEditServicesCommend { get; set; }

        public bool FiveAmountChecked
        {
            get { return _fiveAmountChecked; }
            set { Set(FiveAmountCheckedPropertyName, ref _fiveAmountChecked, value); }
        }

        public bool TenAmountChecked
        {
            get { return _tenAmountChecked; }
            set { Set(TenAmountCheckedPropertyName, ref _tenAmountChecked, value); }
        }

        public ICommand NavigateToBillingMethodViewCommand { get; set; }

        public bool FifteenAmountChecked
        {
            get { return _fifteenAmountChecked; }
            set { Set(FifteenAmountCheckedPropertyName, ref _fifteenAmountChecked, value); }
        }

        public bool TweentyAmountChecked
        {
            get { return _tweentyAmountChecked; }
            set { Set(TweentyAmountCheckedPropertyName, ref _tweentyAmountChecked, value); }
        }

        public new bool IsLoading
        {
            get { return _isLoading; }
            set { Set(IsLoadingPropertyName, ref _isLoading, value); }
        }

        public string PhoneNumber
        {
            get { return _phoneNumber; }
            set { Set(PhoneNumberPropertyName, ref _phoneNumber, value); }
        }

        public int ProgressIndex
        {
            get { return _progressIndex; }
            set { Set(ProgressIndexPropertyName, ref _progressIndex, value); }
        }

        public PersonVO Person
        {
            get { return App.Person; }
        }

        public void Load()
        {
            FiveAmountChecked = true;
        }

        public void SubmitTransfer()
        {
            string amount = string.Empty;

            if (FiveAmountChecked)
                amount = "5";
            else if (TenAmountChecked)
                amount = "10";
            else if (FifteenAmountChecked)
                amount = "15";
            else if (TweentyAmountChecked)
                amount = "20";
            TransferCredit(amount);
        }

        public void NavigateToEditSetvices()
        {
            NavigationService.GoToEditServicesView();
        }

        private void TransferCredit(string transferredAmount)
        {
            //MessageDialog md = new MessageDialog("");

            if (string.IsNullOrEmpty(PhoneNumber))
            {
                MessageBox.Show("please enter a phone number to transfer to");
                return;
            }
            ProgressIndex = 0;
            IsLoading = true;
            bool isConfiremed = false;
            string content = XmlMapper.CreateXmlForCreditTransfer(Person.HashCode, PhoneNumber, transferredAmount);
            var webClientGetBilling = new WebClient();

            webClientGetBilling.Headers[HttpRequestHeader.ContentLength] = content.Length.ToString();
            webClientGetBilling.UploadStringCompleted += webClientGetBilling_UploadStringCompleted;
            webClientGetBilling.UploadProgressChanged += webClient_UploadProgressChanged;
            webClientGetBilling.UploadStringAsync(new Uri(Constants.request_url), "POST", content);
            Progress++;
        }

        private void webClientGetBilling_UploadStringCompleted(object sender, UploadStringCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                MessageBox.Show("Error while performing request");
                IsLoading = false;
                return;
            }
            string error = XmlMapper.GetError(e.Result);
            if (!string.IsNullOrEmpty(error))
            {
                MessageBox.Show(error);
                IsLoading = false;
                return;
            }
            MessageBox.Show("Credit Transfer completed successfully");
            IsLoading = false;
        }

        private void webClient_UploadProgressChanged(object sender, UploadProgressChangedEventArgs e)
        {
            Debug.WriteLine(string.Format("Progress: {0} ", e.ProgressPercentage));
        }

        /// <summary>
        /// NavigateTo BillingMethod View
        /// </summary>
        private void NavigateToBillingMethodView()
        {
            _customNavigationService.GoToBillingMethodView();

        }
    }
}