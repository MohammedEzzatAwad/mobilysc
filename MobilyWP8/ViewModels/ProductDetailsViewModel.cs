﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;
using GalaSoft.MvvmLight.Command;
using MobilyWP8.Common;
using MobilyWP8.Services;

namespace MobilyWP8.ViewModels
{
    public class ProductDetailsViewModel:BaseViewModel
    {
        private readonly IPlansDataHelper _plansDataHelper;
        public ProductDetailsViewModel(IXmlMappers xmlMapper, ICustomNavigationService navigationService, IPlansDataHelper plansDataHelper) : base(xmlMapper, navigationService)
        {
            _plansDataHelper = plansDataHelper;
            LoadedCommand = new RelayCommand(Load);
        }

        private string _imageUrl ;
        private const string ImageUrlPrpertyName = "ImageUrl";
        public string ImageUrl
        {
            get { return _imageUrl; }
            set { Set(ImageUrlPrpertyName, ref _imageUrl, value); }
        }

        private string _content;
        private const string ContentPrpertyName = "Content";
        public string Content
        {
            get { return _content; }
            set { Set(ContentPrpertyName, ref _content, value); }
        }

        public ICommand LoadedCommand { get; set; }
        public void Load()
        {
            var product = _plansDataHelper.GetPlansData().ListOfPlans.SingleOrDefault(p=>Convert.ToInt32(p.Id) == App.Index);
            var currentCulture = Thread.CurrentThread.CurrentCulture;
            Content = currentCulture.Name.Contains("ar") ? product.Content_ar : product.Content_en;
        }
    }
}
