﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using MobilyWP8.Common;
using MobilyWP8.Services;
using MobilyWP8.Resources;
using MobilyWP8.VOs;

namespace MobilyWP8.ViewModels
{
    public class NewsViewModel : BaseViewModel
    {
        private readonly ICustomNavigationService _customNavigationService;
        private readonly IContentChannelHelper _contentChannelHelper;

        public int Index
        {
            get { return App.Index; }
        }
        public PersonVO Person
        {
            get { return App.Person; }
        }

        public ICommand GetNewsDataCommand { get; set; }
        public ICommand NavigateToPromotionsDetailsViewCommand { get; set; }
        public ICommand NavigateToMobilyNewsDetailsViewCommand { get; set; }
        public ICommand NavigateToAlhawaNewsDetailsViewCommand { get; set; }

        public NewsViewModel(IXmlMappers xmlMapper, ICustomNavigationService customNavigationService, IContentChannelHelper contentChannelHelper)
            : base(xmlMapper, customNavigationService)
        {
            _customNavigationService = customNavigationService;
            _contentChannelHelper = contentChannelHelper;

            //Register Commands
            GetNewsDataCommand = new RelayCommand(GetNewsData);
            NavigateToPromotionsDetailsViewCommand = new RelayCommand<NewsPromotionVO>(NavigateToPromotionsDetailsView);
            NavigateToMobilyNewsDetailsViewCommand = new RelayCommand<NewsPromotionVO>(NavigateToMobilyNewsDetailsView);
            NavigateToAlhawaNewsDetailsViewCommand = new RelayCommand<ContentChannel.ContentData>(NavigateToAlhawaNewsDetailsView);
        }

        public async void GetNewsData()
        {
            Progress = 0;
            ProgressMessage = AppResources.LoadingLatestNewsText;
            IsLoading = true;

            //Get Latest News
            var xmlForLogin = XmlMapper.CreateXmlForNewsPromotion(App.Person.HashCode, Constants.GET_LATEST_NEWS_PROMOTION, "1");
            var webClient = new WebClient();
            webClient.Headers[HttpRequestHeader.ContentType] = "application/x-www-form-urlencoded";

            webClient.Headers[HttpRequestHeader.ContentLength] = Convert.ToString(xmlForLogin.Length);
            webClient.UploadStringCompleted += WebClientGetPromotionsUploadStringCompleted;
            webClient.UploadProgressChanged += WebClientUploadProgressChanged;
            webClient.UploadStringAsync(new Uri(Constants.request_url), "POST", xmlForLogin);
        }

        private void WebClientGetPromotionsUploadStringCompleted(object sender, UploadStringCompletedEventArgs e)
        {
            Debug.WriteLine(e.Result);
            
            if (e.Error == null)
            {
                //Fill App.Person.NotificationAndServices with LatestNews
                UpdateUiFrom(e.Result, MessageTypeEnum.LatestNewsMessage);

                //Get Promotions
                ProgressMessage = AppResources.LoadingPromotionsText;
                
                var content = XmlMapper.CreateXmlForNewsPromotion(App.Person.HashCode, Constants.GET_LATEST_NEWS_PROMOTION, "2");
                var webClientGetBilling = new WebClient();

                webClientGetBilling.Headers[HttpRequestHeader.ContentLength] = Convert.ToString(content.Length);
                webClientGetBilling.UploadStringCompleted += WebClientPromotionsUploadStringCompleted;
                webClientGetBilling.UploadProgressChanged += WebClientUploadProgressChanged;
                webClientGetBilling.UploadStringAsync(new Uri(Constants.request_url), "POST", content);
            }
        }

        void WebClientPromotionsUploadStringCompleted(object sender, UploadStringCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                UpdateUiFrom(e.Result, MessageTypeEnum.PromotionsMessage);
                GetContentChannel();
                IsLoading = false;
            }
        }

        private void WebClientUploadProgressChanged(object sender, UploadProgressChangedEventArgs e)
        {
            Progress++;
            Debug.WriteLine(string.Format("Progress: {0} ", e.ProgressPercentage));
        }

        /// <summary>
        /// For News3alhawa ContentType equals "1"
        /// For Sports3alhawa ContentType equals "2"
        /// </summary>
        private void GetContentChannel()
        {
            var contentChannel = _contentChannelHelper.GetContentChannel();
            var currentCulture = Thread.CurrentThread.CurrentCulture;
            switch (currentCulture.Name)
            {
                case "ar-SA":
                    App.Person.AlhawaChannels.News3alhawa = contentChannel.ContentChannelAr.Where(m => m.ContentType == "1");
                    App.Person.AlhawaChannels.Sports3alhawa = contentChannel.ContentChannelAr.Where(m => m.ContentType == "2");
                    break;
                case "en-US":
                    App.Person.AlhawaChannels.News3alhawa = contentChannel.ContentChannelEn.Where(m => m.ContentType == "1");
                    App.Person.AlhawaChannels.Sports3alhawa = contentChannel.ContentChannelEn.Where(m => m.ContentType == "2");
                    break;
            }
        }

        /// <summary>
        /// Navigate To LatestNewsDetails View
        /// </summary>
        private void NavigateToPromotionsDetailsView(NewsPromotionVO item)
        {
            App.SelectedPromotionsNews = item;
            _customNavigationService.GoToPromotionsDetailsView();
        }

        /// <summary>
        /// Navigate To MobilyNewsDetails View
        /// </summary>
        /// <param name="item"></param>
        private void NavigateToMobilyNewsDetailsView(NewsPromotionVO item)
        {
            App.SelectedMobilyNews = item;
            _customNavigationService.GoToMobilyNewsDetailsView();
        }

        /// <summary>
        /// Navigate To AlhawaNewsDetails View
        /// </summary>
        private void NavigateToAlhawaNewsDetailsView( ContentChannel.ContentData item)
        {
            App.SelectedAlhawaNews = item;
            _customNavigationService.GoToAlhawaNewsDetailsView();
        }
    }
}
