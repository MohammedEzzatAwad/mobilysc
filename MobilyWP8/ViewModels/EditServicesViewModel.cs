﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using GalaSoft.MvvmLight.Command;
using MobilyWP8.Common;
using MobilyWP8.Services;
using MobilyWP8.VOs;

namespace MobilyWP8.ViewModels
{
    public class EditServicesViewModel:BaseViewModel
    {
        public EditServicesViewModel(IXmlMappers xmlMapper, ICustomNavigationService navigationService) : base(xmlMapper, navigationService)
        {
            ToogleChangeCommand = new RelayCommand<ManagedServices>(ToogleChange);
        }
        public ICommand ToogleChangeCommand { get; set; }

        private string _progressName;
        private const string ProgressNamePropertyName= "ProgressName";
        public string ProgressName { get { return _progressName; } set { Set(ProgressNamePropertyName, ref _progressName, value); } }

        public void ToogleChange(ManagedServices selectedService)
        {
            string action = string.Empty;
            IsLoading = true;
            ProgressName = "Requesting update from server";
            Progress++;
            //I added because i need the changed value not the original value
            if (selectedService.Status)
            {
                switch (selectedService.ServiceName)
                {
                    case "SMS_Bundle":
                        action = "unsubscribe";
                        break;
                    case "MMS_Bundle":
                        action = "disable";
                        break;
                    case "GPRS":
                        action = "unsubscribe";
                        break;
                    case "RBT":
                        action = "unsubscribe";
                        break;
                    case "International_Barring":
                        action = "Disable";
                        break;
                    case "Gaming":
                        action = "disGaming";
                        break;
                    case "Video_Telephony":
                        action = "disVideoTell";
                        break;
                    case "Video_Streaming":
                        action = "disVideoStrm";
                        break;
                    case "PTT":
                        action = "unsubscribe";
                        break;
                    case "Voice_SMS":
                        action = "unsubscribe";
                        break;
                    case "ICS2":
                        action = "unsubscribe";
                        break;
                    case "LBS":
                        action = "disable";
                        break;
                    case "MOBILY_TV":
                        action = "disable";
                        break;
                    case "WIFI_Roaming":
                        action = "unsubscribe";
                        break;
                    default:
                        //App.ViewModelLocator.ShellViewModel.ErrorMessage = "Unknown Error";
                        break;
                }


            }
            else
            {
                switch (selectedService.ServiceName)
                {
                    case "SMS_Bundle":
                        action = "subscribe";
                        break;
                    case "MMS_Bundle":
                        action = "enable";
                        break;
                    case "GPRS":
                        action = "subscribe";
                        break;
                    case "RBT":
                        action = "subscribe";
                        break;
                    case "International_Barring":
                        action = "enable";
                        break;
                    case "Gaming":
                        action = "enGaming";
                        break;
                    case "Video_Telephony":
                        action = "enVideoTell";
                        break;
                    case "Video_Streaming":
                        action = "enVideoStrm";
                        break;
                    case "PTT":
                        action = "subscribe";
                        break;
                    case "Voice_SMS":
                        action = "subscribe";
                        break;
                    case "ICS2":
                        action = "subscribe";
                        break;
                    case "LBS":
                        action = "enable";
                        break;
                    case "MOBILY_TV":
                        action = "enable";
                        break;
                    case "WIFI_Roaming":
                        action = "subscribe";
                        break;
                    default:
                        //App.ViewModelLocator.ShellViewModel.ErrorMessage = "Unknown Error";
                        break;
                }
            }
            var content = XmlMapper.CreateXmlForSupplemanterySubscription(App.Person.HashCode,action,selectedService.ServiceName,"");
            var webClientSupplementaryEdit = new WebClient();
            webClientSupplementaryEdit.Headers[HttpRequestHeader.ContentLength] = content.Length.ToString();
            webClientSupplementaryEdit.UploadStringCompleted += webClientSupplementaryEdit_UploadStringCompleted;
            webClientSupplementaryEdit.UploadStringAsync(new Uri(Constants.request_url), "POST", content);
        }

        void webClientSupplementaryEdit_UploadStringCompleted(object sender, UploadStringCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                Progress++;
                ProgressName = "Updating list";

                var error = XmlMapper.GetError(e.Result);
                if (!string.IsNullOrEmpty(error))
                {
                    MessageBox.Show(error);
                    Progress++;
                    ProgressName = "Done";
                    IsLoading = false;
                    Progress = 0;
                    return;
                }
                var content = XmlMapper.CreateXmlForBalanceBillNeqaty(App.Person.HashCode, Constants.SUPPLEMENTARY_SERVICES_INQUIRY);
                var webClientSupplementary = new WebClient();
                webClientSupplementary.Headers[HttpRequestHeader.ContentLength] = content.Length.ToString();
                webClientSupplementary.UploadStringCompleted += webClientSupplementary_UploadStringCompleted;
                webClientSupplementary.UploadStringAsync(new Uri(Constants.request_url), "POST", content);
                
            }

        }

        void webClientSupplementary_UploadStringCompleted(object sender, UploadStringCompletedEventArgs e)
        {
            Progress++;
            ProgressName = "Done";
            if (e.Error == null)
            {
                UpdateUiFrom(e.Result, MessageTypeEnum.SupplementaryServiceMessage);

                
            }
            IsLoading = false;
            Progress = 0;
        }
        public PersonVO Person { get { return App.Person; } }
    }
}
