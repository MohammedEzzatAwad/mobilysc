﻿using System.Windows.Input;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using MobilyWP8.Services;

namespace MobilyWP8.ViewModels
{
    public class MainViewModel : ViewModelBase
    {
        private const string ProductIndexPropertyName = "ProductIndex";
        private const string NewsIndexPropertyName = "NewsIndex";
        private readonly ICustomNavigationService _customNavigationService;
        private int _productIndex;
        private int _newsIndex;

        public MainViewModel(ICustomNavigationService customNavigationService)
        {
            _customNavigationService = customNavigationService;

            //Register Commands
            NavigateToProductAndServicesCommand = new RelayCommand(NavigateToProductAndServices);
            NavigateToNewsViewCommand = new RelayCommand(NavigateToNewsView);
            NavigateToLogInCommand = new RelayCommand(NavigateToLogIn);
            NavigateToMyAccountViewCommand = new RelayCommand<int>(NavigateToMyAccountView);
            LoadedCommand = new RelayCommand(Load);
            
        }

       

        public ICommand NavigateToProductAndServicesCommand { get; set; }

        public int ProductIndex
        {
            get { return _productIndex; }
            set
            {
                //App.Index = ProductIndex;
                Set(ProductIndexPropertyName, ref _productIndex, value);
                //_customNavigationService.GoToProductAndServices(_productIndex);
            }
        }

        public ICommand LoadedCommand { get; set; }

        private void Load()
        {
            ProductIndex = NewsIndex = -1;
        }

        public int NewsIndex
        {
            get { return _newsIndex; }
            set
            {
                //App.Index = _newsIndex;
                Set(NewsIndexPropertyName, ref _newsIndex, value);
                //_customNavigationService.GoToNewsView();
            }
        }

        public ICommand NavigateToMyAccountViewCommand { get; set; }
        public void NavigateToMyAccountView(int index)
        {
            App.Index = index;
            _customNavigationService.GoToMyAccountView();
        }

        public ICommand NavigateToNewsViewCommand { get; set; }

        private void NavigateToProductAndServices()
        {
            if (ProductIndex>-1)
            {
                App.Index = ProductIndex;
                _customNavigationService.GoToProductAndServices();
            }
        }

        private void NavigateToNewsView()
        {
            if (NewsIndex > -1)
            {
                App.Index = NewsIndex;
                _customNavigationService.GoToNewsView(); 
            }
        }

        public ICommand NavigateToLogInCommand { get; set; }

        public void NavigateToLogIn()
        {
            _customNavigationService.GoToLoginPage();
        }
    }
}