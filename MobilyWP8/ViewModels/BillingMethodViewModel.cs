﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using GalaSoft.MvvmLight.Command;
using MobilyWP8.Common;
using MobilyWP8.Resources;
using MobilyWP8.Services;
using MobilyWP8.VOs;

namespace MobilyWP8.ViewModels
{
    public class BillingMethodViewModel : BaseViewModel
    {
        public bool IsEmailChecked { get; set; }
        
        public BillMethodStatus BillMethodStatus
        {
            get { return App.Person.BillMethod.BillMethodStatus; }
            set { App.Person.BillMethod.BillMethodStatus = value; }
        }

        public string Email
        {
            get { return App.Person.BillMethod.Email; }
            set { App.Person.BillMethod.Email = value; }
        }
        
        public ICommand ChangeBillingMethodCommand { get; set; }

        public BillingMethodViewModel(IXmlMappers xmlMapper, ICustomNavigationService navigationService) : base(xmlMapper, navigationService)
        {
            //Register Commands
            ChangeBillingMethodCommand = new RelayCommand(ChangeBillingMethod);
        }

        /// <summary>
        /// Change Billing Method
        /// </summary>
        private void ChangeBillingMethod()
        {
            Progress = 0;
            ProgressMessage = AppResources.WorkingOnYourRequestText;
            IsLoading = true;

            if (IsEmailChecked)
            {
                BillMethodStatus = BillMethodStatus.EMAIL;

                if (!string.IsNullOrEmpty(Email))
                {
                    var reStrict = new Regex(Constants.PatternEmail);

                    if (reStrict.IsMatch(Email))
                    {
                        ChangeBillMethod(BillMethodStatus.ToString(), Email);
                    }
                    else
                    {
                        MessageBox.Show(AppResources.InvalidEmailAddressError);
                        return;
                    }
                }
                else
                {
                    MessageBox.Show(AppResources.EmptyEmailAddressError);
                    return;
                }
            }
            else
            {
                BillMethodStatus = BillMethodStatus.POBOX;
                ChangeBillMethod(BillMethodStatus.ToString());
            }
        }
        
        public async void ChangeBillMethod(string InvoiceMechanism, string email = "")
        {
            var xmlForBillMethod = XmlMapper.CreateXmlForBillMethodUpdate(App.Person.HashCode, InvoiceMechanism, email);
            var webClient = new WebClient();
            webClient.Headers[HttpRequestHeader.ContentType] = "application/x-www-form-urlencoded";

            webClient.Headers[HttpRequestHeader.ContentLength] = Convert.ToString(xmlForBillMethod.Length);
            webClient.UploadStringCompleted += WebClientBillMethodUploadStringCompleted;
            webClient.UploadProgressChanged += WebClientUploadProgressChanged;
            webClient.UploadStringAsync(new Uri(Constants.request_url), "POST", xmlForBillMethod);
        }

        private void WebClientUploadProgressChanged(object sender, UploadProgressChangedEventArgs e)
        {
            Progress++;
            Debug.WriteLine(string.Format("Progress: {0} ", e.ProgressPercentage));
        }

        private void WebClientBillMethodUploadStringCompleted(object sender, UploadStringCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                if (IsEmailChecked)
                {
                    MessageBox.Show(AppResources.TheOperationWasCompletedSuccessfullyText + "!\n" + Constants.EmailMessage);
                }
                else
                {
                    MessageBox.Show(AppResources.TheOperationWasCompletedSuccessfullyText + "!\n" + Constants.PostalMessage);
                }
            }
            IsLoading = false;
        }
    }
}
