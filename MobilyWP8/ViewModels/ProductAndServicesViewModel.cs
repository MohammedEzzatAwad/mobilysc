﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Input;
using GalaSoft.MvvmLight.Command;
using MobilyWP8.Common;
using MobilyWP8.Services;

namespace MobilyWP8.ViewModels
{
    public class ProductAndServicesViewModel : BaseViewModel
    {
        private const string IndexPropertyName = "Index";
        private const string ProductIndexPropertyName = "SelectedProduct";
        private const string PostPaidPlansPropertyName = "PostPaidPlans";
        private const string PrepaidPlansPropertyName = "PrepaidPlans";
        private const string ServicesPropertyName = "Plans";
        private const string ArabicVisiblePropertyName = "ArabicVisible";
        private const string EnglishVisiblePropertyName = "EnglishVisible";
        private readonly IPlansDataHelper _plansDataHelper;
        private bool _arabicVisible;
        private bool _englishVisible;
        private int _index;
        private List<Plans.Plan> _postPaidPlans;
        private List<Plans.Plan> _prepaidPlans;

        private Plans.Plan _selectedProduct;
        private List<Plans.Plan> _services;

        public ProductAndServicesViewModel(IXmlMappers xmlMapper, ICustomNavigationService navigationService,
                                           IPlansDataHelper plansDataHelper)
            : base(xmlMapper, navigationService)
        {
            _plansDataHelper = plansDataHelper;
            LoadedCommand = new RelayCommand(Load);
            NavigateToProductDetailsCommand = new RelayCommand(NavigateToProductDetails);
        }

        public Plans.Plan SelectedProduct
        {
            get { return _selectedProduct; }
            set { Set(ProductIndexPropertyName, ref _selectedProduct, value); }
        }

        public ICommand NavigateToProductDetailsCommand { get; set; }
    

        public List<Plans.Plan> PostPaidPlans
        {
            get { return _postPaidPlans; }
            set { Set(PostPaidPlansPropertyName, ref _postPaidPlans, value); }
        }

        public List<Plans.Plan> PrepaidPlans
        {
            get { return _prepaidPlans; }
            set { Set(PrepaidPlansPropertyName, ref _prepaidPlans, value); }
        }

        public List<Plans.Plan> Services
        {
            get { return _services; }
            set { Set(ServicesPropertyName, ref _services, value); }
        }

        public bool ArabicVisible
        {
            get { return _arabicVisible; }
            set { Set(ArabicVisiblePropertyName, ref _arabicVisible, value); }
        }

        public bool EnglishVisible
        {
            get { return _englishVisible; }
            set { Set(EnglishVisiblePropertyName, ref _englishVisible, value); }
        }

        public ICommand LoadedCommand { get; set; }

        public int Index
        {
            get { return App.Index; }
        }

        public void NavigateToProductDetails()
        {
            if (_selectedProduct!=null)
            {
                App.Index = Convert.ToInt32(_selectedProduct.Id);
                NavigationService.GoToProductDetailsView(); 
            }
        }

        public void Load()
        {
            List<Plans.Plan> all = _plansDataHelper.GetPlansData().ListOfPlans;
            Services = all.Where(p => p.Parent == "0").ToList();
            PrepaidPlans = all.Where(p => p.Parent == "1").ToList();
            PostPaidPlans = all.Where(p => p.Parent == "2").ToList();
        }
    }
}