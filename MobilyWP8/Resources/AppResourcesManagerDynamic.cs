﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Resources;
using System.Text;
using System.Threading.Tasks;

namespace MobilyWP8.Resources
{
    public class AppResourcesManagerDynamic : INotifyPropertyChanged
    {
        private AppResources appResources;

        public AppResources AppResources
        {
            get { return appResources; }
        }

        public AppResourcesManagerDynamic()
        {
            appResources = new AppResources();
        }

        public void ResetResources()
        {
            OnPropertyChanged(() => AppResources);
        }

        public static string GetString(string item)
        {
            var rm = new ResourceManager("MobilyWP8.Resources.AppResources", Assembly.GetExecutingAssembly());
            return rm.GetString(item) ?? item;
        }

        #region INotifyPropertyChanged region
        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged<T>(Expression<Func<T>> selector)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(GetPropertyNameFromExpression(selector)));
            }
        }

        public static string GetPropertyNameFromExpression<T>(Expression<Func<T>> property)
        {
            var lambda = (LambdaExpression)property;
            MemberExpression memberExpression;

            if (lambda.Body is UnaryExpression)
            {
                var unaryExpression = (UnaryExpression)lambda.Body;
                memberExpression = (MemberExpression)unaryExpression.Operand;
            }
            else
            {
                memberExpression = (MemberExpression)lambda.Body;
            }

            return memberExpression.Member.Name;
        }
        #endregion
    }
}
