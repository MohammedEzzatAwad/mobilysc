﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Ioc;
using Microsoft.Practices.ServiceLocation;
using MobilyWP8.Services;
using MobilyWP8.ViewModels;

namespace MobilyWP8.Common
{
    public class ViewModelLocator
    {
        static ViewModelLocator()
        {
            ServiceLocator.SetLocatorProvider(() => SimpleIoc.Default);

            if (ViewModelBase.IsInDesignModeStatic)
            {
                //SimpleIoc.Default.Register<IDataService, Design.DesignDataService>();
            }
            else
            {
                SimpleIoc.Default.Register<ICustomNavigationService, CustomNavigationService>();
                SimpleIoc.Default.Register<IXmlMappers, XmlMappers>();
                SimpleIoc.Default.Register<IPlansDataHelper, PlansDataHelper>();
                SimpleIoc.Default.Register<IContentChannelHelper, ContentChannelHelper>();
                //SimpleIoc.Default.Register<INavigationService,NavigationService>();
            }

            //Registering ViewModels
            SimpleIoc.Default.Register<MainViewModel>();
            SimpleIoc.Default.Register<LoginViewModel>();
            SimpleIoc.Default.Register<NewsViewModel>();
            SimpleIoc.Default.Register<LatestNewsViewModel>();
            SimpleIoc.Default.Register<AlhawaNewsViewModel>();
            SimpleIoc.Default.Register<AppSettingsViewModel>();
            SimpleIoc.Default.Register<ProductAndServicesViewModel>();
            SimpleIoc.Default.Register<ProductDetailsViewModel>();
            SimpleIoc.Default.Register<MyAccountViewModel>();
            SimpleIoc.Default.Register<BillingMethodViewModel>();
            SimpleIoc.Default.Register<EditServicesViewModel>();
        }


        /// <summary>
        /// Gets the Main property.
        /// </summary>
        public MainViewModel Main
        {
            get { return ServiceLocator.Current.GetInstance<MainViewModel>(); }
        }

        /// <summary>
        /// Gets the Main property.
        /// </summary>
        public LoginViewModel Login
        {
            get { return ServiceLocator.Current.GetInstance<LoginViewModel>(); }
        }


        /// <summary>
        /// Gets the News property.
        /// </summary>
        public NewsViewModel News
        {
            get { return ServiceLocator.Current.GetInstance<NewsViewModel>(); }
        }

        /// <summary>
        /// Gets the LatestNews property.
        /// </summary>
        public LatestNewsViewModel LatestNews
        {
            get { return ServiceLocator.Current.GetInstance<LatestNewsViewModel>(); }
        }

        /// <summary>
        /// Gets the LatestNews property.
        /// </summary>
        public AlhawaNewsViewModel AlhawaNews
        {
            get { return ServiceLocator.Current.GetInstance<AlhawaNewsViewModel>(); }
        }

        /// <summary>
        /// Gets the settings view model
        /// </summary>
        public AppSettingsViewModel Settings
        {
            get { return ServiceLocator.Current.GetInstance<AppSettingsViewModel>(); }
        }

        public ProductAndServicesViewModel ProductAndServices
        {
            get { return ServiceLocator.Current.GetInstance<ProductAndServicesViewModel>(); }
        }


        /// <summary>
        /// Get the prodcut Detail view model
        /// </summary>
        public ProductDetailsViewModel ProductDetails
        {
            get { return ServiceLocator.Current.GetInstance<ProductDetailsViewModel>(); }
        }


        public MyAccountViewModel MyAccount
        {
            get { return ServiceLocator.Current.GetInstance<MyAccountViewModel>(); }
        }

        public BillingMethodViewModel BillingMethod
        {
            get { return ServiceLocator.Current.GetInstance<BillingMethodViewModel>(); }
        }

        public EditServicesViewModel EditServices
        {
            get { return ServiceLocator.Current.GetInstance<EditServicesViewModel>(); }
        }
    }
}