﻿using MobilyWP8.Resources;
using System;
using System.Diagnostics;
using System.Xml.Linq;

namespace MobilyWP8.Common
{
    public interface IXmlMappers
    {
        string CreateXmlForLogin(string userName, string password);
        string CreateXmlForLogout(string hashcode);

        string CreateXmlForCreditTransfer(string hashcode, string recipientMSISDN,
                                                          string transferredAmount);

        bool CreditTransferConfirmation(string msg);
        string CreateXmlForBalanceBillNeqaty(string hashcode, string cstRequest);
        string GetHashCode(string msg);
        string CreateXmlForNewsPromotion(string hashcode, string cstRequest, string type);
        string CreateXmlForBillMethodUpdate(string hashcode, string invoiceMechanism, string email);

        string CreateXmlForSupplemanterySubscription(string hashcode, string action, string serviceName,
                                                                     string serviceType);

        string CreateXmlForRedeemNeqatyPoints(string hashcode, string ItemCode);
        string GetError(string msg, bool isCreditTransferOrSupplemntaryServices = false);
    }

    public class XmlMappers : IXmlMappers
    {

        public  string CreateXmlForLogin(string userName, string password)
        {
           
            XElement xelement = new XElement((XName)Constants.login_MOBILY_IPHONE_REQUEST);
            xelement.Add(new XElement((XName)Constants.login_FunctionId, (object)Constants.login_LOGIN));
            xelement.Add(new XElement((XName)Constants.login_device, (object)Constants.login_device_number));
            xelement.Add(new XElement((XName)Constants.login_UserName, userName));
            xelement.Add(new XElement((XName)Constants.login_Password, password));
            xelement.Add(new XElement((XName)Constants.login_RequestorLanguage,
                                     Constants.login_EA));
            Debug.WriteLine(((object)xelement).ToString());
            return ((object)xelement).ToString();
        }

        public string CreateXmlForLogout(string hashcode)
        {
            
            XElement xelement = new XElement(Constants.login_MOBILY_IPHONE_REQUEST);
            xelement.Add(new XElement(Constants.login_FunctionId, Constants.login_LOGOUT));
            xelement.Add(new XElement(Constants.login_device, Constants.login_device_number));
            xelement.Add(new XElement(Constants.login_HashCode, hashcode));
            xelement.Add(new XElement(Constants.login_RequestorLanguage, Constants.login_EA));
            return ((object)xelement).ToString();
        }

        public string CreateXmlForCreditTransfer(string hashcode, string recipientMSISDN,
                                                          string transferredAmount)
        {
           
            XElement xelement = new XElement(Constants.login_MOBILY_IPHONE_REQUEST);
            xelement.Add(new XElement(Constants.login_FunctionId, Constants.CREDIT_TRANSFER));
            xelement.Add(new XElement(Constants.login_device, Constants.login_device_number));
            xelement.Add(new XElement(Constants.login_HashCode, hashcode));
            xelement.Add(new XElement(Constants.RecipientMSISDN, Constants.RecipientMSISDN));
            xelement.Add(new XElement(Constants.TransferredAmount, Constants.TransferredAmount));
            xelement.Add(new XElement(Constants.login_RequestorLanguage, Constants.login_EA));
            return ((object)xelement).ToString();
        }

        public  bool CreditTransferConfirmation(string msg)
        {
            string str = string.Empty;
            msg = msg.Replace("---!>", "-->");
            foreach (
                XElement xelement1 in XElement.Parse(Constants.login_root + msg + Constants.login_rootEnd).Descendants()
                )
            {
                if (xelement1.Name == (XName)Constants.login_MOBILY_IPHONE_REPLY)
                {
                    foreach (XElement xelement2 in xelement1.Descendants())
                    {
                        if (xelement2.Name == (XName)Constants.ConfirmationKey)
                        {
                            if (xelement2.Value == "1001")
                                return true;
                        }
                    }
                }
            }
            return false;
        }

        public  string CreateXmlForBalanceBillNeqaty(string hashcode, string cstRequest)
        {
             
            XElement xelement = new XElement(Constants.login_MOBILY_IPHONE_REQUEST);
            xelement.Add(new XElement(Constants.login_FunctionId, cstRequest));
            xelement.Add(new XElement(Constants.login_device, Constants.login_device_number));
            xelement.Add(new XElement(Constants.login_HashCode, hashcode));
            xelement.Add(new XElement(Constants.login_RequestorLanguage, Constants.login_EA));
            return ((object)xelement).ToString();
        }

        public  string GetHashCode(string msg)
        {
            string str = string.Empty;
            msg = msg.Replace("---!>", "-->");
            foreach (
                XElement xelement1 in XElement.Parse(Constants.login_root + msg + Constants.login_rootEnd).Descendants()
                )
            {
                if (xelement1.Name == (XName)Constants.login_MOBILY_IPHONE_REPLY)
                {
                    foreach (XElement xelement2 in xelement1.Descendants())
                    {
                        if (xelement2.Name == (XName)Constants.login_HashCode)
                            return xelement2.Value;
                    }
                }
            }
            return null;
        }

        public  string CreateXmlForNewsPromotion(string hashcode, string cstRequest, string type)
        {
            var xelement = new XElement(Constants.login_MOBILY_IPHONE_REQUEST);
            xelement.Add(new XElement(Constants.login_FunctionId, cstRequest));
            xelement.Add(new XElement(Constants.login_device, Constants.login_device_number));
            xelement.Add(new XElement(Constants.login_HashCode, hashcode));
            xelement.Add(new XElement(Constants.NewsPromotion_Id), "1");
            xelement.Add(new XElement(Constants.NewsPromotion_Type, type));
            xelement.Add(new XElement(Constants.login_RequestorLanguage, Constants.login_EA));
            return (xelement).ToString();
        }

        public  string CreateXmlForBillMethodUpdate(string hashcode, string invoiceMechanism, string email)
        {
            var xelement = new XElement(Constants.login_MOBILY_IPHONE_REQUEST);
            xelement.Add(new XElement(Constants.login_FunctionId, Constants.BILL_METHOD_Email_Invoice));
            xelement.Add(new XElement(Constants.login_device, Constants.login_device_number));
            xelement.Add(new XElement(Constants.login_HashCode, hashcode));
            xelement.Add(new XElement(Constants.login_RequestorLanguage, Constants.balance_E));
            xelement.Add(new XElement(Constants.BILL_METHOD_InvoiceMechanism, invoiceMechanism));
            xelement.Add(new XElement(Constants.BILL_METHOD_Email, email));
            return (xelement).ToString();
        }

        public  string CreateXmlForSupplemanterySubscription(string hashcode, string action, string serviceName,
                                                                   string serviceType)
        { 
            var xelement = new XElement(Constants.login_MOBILY_IPHONE_REQUEST);
            xelement.Add(new XElement(Constants.login_FunctionId, Constants.SupplementaryService_SUPPLEMENTARY_SERVICE));
            xelement.Add(new XElement(Constants.login_device, Constants.login_device_number));
            xelement.Add(new XElement(Constants.login_HashCode, hashcode));
            xelement.Add(new XElement(Constants.SupplementaryService_Action, action));
            xelement.Add(new XElement(Constants.SupplementaryService_ServiceName, serviceName));
            xelement.Add(new XElement(Constants.SupplementaryService_ServiceType, serviceType));
            xelement.Add(new XElement(Constants.login_RequestorLanguage, Constants.login_EA));
            return (xelement).ToString();
        }

        public  string CreateXmlForRedeemNeqatyPoints(string hashcode, string ItemCode)
        {
            
            var xelement = new XElement(Constants.login_MOBILY_IPHONE_REQUEST);
            xelement.Add(new XElement(Constants.login_FunctionId, Constants.neqaty_LOYALTY_REDEMPTION));
            xelement.Add(new XElement(Constants.login_device, Constants.login_device_number));
            xelement.Add(new XElement(Constants.login_HashCode, hashcode));
            xelement.Add(new XElement(Constants.neqaty_ItemCode, ItemCode));
            xelement.Add(new XElement(Constants.login_RequestorLanguage, Constants.login_EA));
            return (xelement).ToString();
        }

        public  string GetError(string msg, bool isCreditTransferOrSupplemntaryServices = false)
        {
            var flag = false;
            var str = string.Empty;
            msg = msg.Replace("---!>", "-->");
            foreach (XElement xelement1 in XElement.Parse(Constants.login_root + msg + Constants.login_rootEnd).Descendants())
            {
                if (xelement1.Name == (XName)Constants.login_MOBILY_IPHONE_REPLY)
                {
                    flag = true;
                    foreach (XElement xelement2 in xelement1.Descendants())
                    {
                        if (xelement2.Name == (XName)Constants.login_ERROR)
                        {
                            foreach (XElement xelement3 in xelement2.Descendants())
                            {
                                if (xelement3.Name == (XName)Constants.login_ErrorCode)
                                {
                                    if (!string.IsNullOrEmpty(xelement3.Value))
                                    {
                                        if (!string.IsNullOrEmpty(xelement3.Value) && xelement3.Value != "0000")
                                        {
                                            var sErrorCode = "EC" + xelement3.Value;

                                            str = AppResourcesManagerDynamic.GetString(sErrorCode);
                                        }

                                        //switch (int.Parse(xelement3.Value))
                                        //{
                                        //    case 5001:
                                        //        str = Constants.error_5001;
                                        //        break;
                                        //    case 5002:
                                        //        str = Constants.error_5002;
                                        //        break;
                                        //    case 10000:
                                        //        str = Constants.error_10000;
                                        //        break;
                                        //    case 10001:
                                        //        str = Constants.error_10001;
                                        //        break;
                                        //    case 10002:
                                        //        str = Constants.error_10002;
                                        //        break;
                                        //    case 10003:
                                        //        str = Constants.error_10003;
                                        //        break;
                                        //    case 10004:
                                        //        str = Constants.error_10004;
                                        //        break;
                                        //    case 10005:
                                        //        str = Constants.error_10005;
                                        //        break;
                                        //    case 10191:
                                        //        str = Constants.error_10191;
                                        //        break;
                                        //    case 10008:
                                        //        str = Constants.error_10008;
                                        //        break;
                                        //    case 100009:
                                        //        str = Constants.error_100009;
                                        //        break;
                                        //    case 1320:
                                        //        str = Constants.error_1320;
                                        //        break;
                                        //    case 1315:
                                        //        str = Constants.error_1315;
                                        //        break;
                                        //    case 1321:
                                        //        str = Constants.error_1321;
                                        //        break;
                                        //    case 1314:
                                        //        str = Constants.error_1314;
                                        //        break;
                                        //    case 1313:
                                        //        str = Constants.error_1313;
                                        //        break;
                                        //    case 1322:
                                        //        str = Constants.error_1322;
                                        //        break;
                                        //    case 1323:
                                        //        str = Constants.error_1323;
                                        //        break;
                                        //    case 8103:
                                        //        str = Constants.error_8104;
                                        //        break;
                                        //    case 8104:
                                        //        str = Constants.error_8104;
                                        //        break;
                                        //    case 8125:
                                        //        str = Constants.error_8125;
                                        //        break;
                                        //    case 8129:
                                        //        str = Constants.error_8129;
                                        //        break;
                                        //    case 8122:
                                        //        str = Constants.error_8122;
                                        //        break;
                                        //    case 4305:
                                        //        str = Constants.error_4305;
                                        //        break;
                                        //    case 4304:
                                        //        str = Constants.error_4304;
                                        //        break;
                                        //    case 9751:
                                        //        str = Constants.error_9751;
                                        //        break;
                                        //    case 12060:
                                        //        str = Constants.error_12060;
                                        //        break;
                                        //    case 12054:
                                        //        str = Constants.error_12054;
                                        //        break;
                                        //    case 12052:
                                        //        str = Constants.error_12052;
                                        //        break;
                                        //    case 4306:
                                        //        str = Constants.error_4306;
                                        //        break;
                                        //    case 1325:
                                        //        str = Constants.error_1325;
                                        //        break;

                                        //    case 12055:
                                        //        str = Constants.error_12055;
                                        //        break;

                                        //    case 1:
                                        //        str = Constants.error_1;
                                        //        break;
                                        //    case 2:
                                        //        str = Constants.error_2;
                                        //        break;
                                        //    case 3:
                                        //        str = Constants.error_3;
                                        //        break;
                                        //    case 4:
                                        //        str = Constants.error_4;
                                        //        break;
                                        //    case 6:
                                        //        str = Constants.error_6;
                                        //        break;
                                        //    case 11:
                                        //        str = Constants.error_11;
                                        //        break;
                                        //    case 12:
                                        //        str = Constants.error_12;
                                        //        break;
                                        //    case 13:
                                        //        str = Constants.error_13;
                                        //        break;
                                        //    case 25:
                                        //        str = Constants.error_25;
                                        //        break;
                                        //    case 28:
                                        //        str = Constants.error_28;
                                        //        break;
                                        //    case 36:
                                        //        str = Constants.error_36;
                                        //        break;
                                        //    case 38:
                                        //        str = Constants.error_38;
                                        //        break;
                                        //    case 8123:
                                        //        str = Constants.error_8123;
                                        //        break;
                                        //    case 6000:
                                        //        str = Constants.error_6000;
                                        //        break;
                                        //    case 10193:
                                        //        str = Constants.error_10193;
                                        //        break;
                                        //    case 10195:
                                        //        str = Constants.error_10195;
                                        //        break;
                                        //    case 10196:
                                        //        str = Constants.error_10196;
                                        //        break;
                                        //    case 10197:
                                        //        str = Constants.error_10197;
                                        //        break;
                                        //    case 10203:
                                        //        str = Constants.error_10203;
                                        //        break;
                                        //    case 7000:
                                        //        str = Constants.error_7000;
                                        //        break;
                                        //    case 7001:
                                        //        str = Constants.error_7001;
                                        //        break;
                                        //    case 10194:
                                        //        str = Constants.error_10194;
                                        //        break;
                                        //    case 1008:
                                        //        str = Constants.error_1008;
                                        //        break;
                                        //    case 10009:
                                        //        str = Constants.error_10009;
                                        //        break;
                                        //    case 1900:
                                        //        str = Constants.error_1900;
                                        //        break;
                                        //    case 2551:
                                        //        str = Constants.error_2551;
                                        //        break;
                                        //    case 2552:
                                        //        str = Constants.error_2552;
                                        //        break;
                                        //    case 2553:
                                        //        str = Constants.error_2553;
                                        //        break;
                                        //    case 2554:
                                        //        str = Constants.error_2554;
                                        //        break;
                                        //    case 2555:
                                        //        str = Constants.error_2555;
                                        //        break;
                                        //    case 2556:
                                        //        str = Constants.error_2556;
                                        //        break;
                                        //    case 2557:
                                        //        str = Constants.error_2557;
                                        //        break;
                                        //    case 2558:
                                        //        str = Constants.error_2558;
                                        //        break;
                                        //    case 2559:
                                        //        str = Constants.error_2559;
                                        //        break;
                                        //    case 2560:
                                        //        str = Constants.error_2560;
                                        //        break;
                                        //    case 2561:
                                        //        str = Constants.error_2561;
                                        //        break;
                                        //    case 2562:
                                        //        str = Constants.error_2562;
                                        //        break;
                                        //    case 2563:
                                        //        str = Constants.error_2563;
                                        //        break;
                                        //    case 2564:
                                        //        str = Constants.error_2564;
                                        //        break;
                                        //    case 2565:
                                        //        str = Constants.error_2565;
                                        //        break;
                                        //    case 2566:
                                        //        str = Constants.error_2566;
                                        //        break;
                                        //    case 2567:
                                        //        str = Constants.error_2567;
                                        //        break;
                                        //    case 2568:
                                        //        str = Constants.error_2568;
                                        //        break;
                                        //    case 2569:
                                        //        str = Constants.error_2569;
                                        //        break;
                                        //    case 2570:
                                        //        str = Constants.error_2570;
                                        //        break;
                                        //    case 2571:
                                        //        str = Constants.error_2571;
                                        //        break;
                                        //    case 2572:
                                        //        str = Constants.error_2572;
                                        //        break;
                                        //    case 2573:
                                        //        str = Constants.error_2573;
                                        //        break;
                                        //    case 2574:
                                        //        str = Constants.error_2574;
                                        //        break;
                                        //    case 2575:
                                        //        str = Constants.error_2575;
                                        //        break;
                                        //    case 2576:
                                        //        str = Constants.error_2576;
                                        //        break;

                                        //    case 2577:
                                        //        str = Constants.error_2577;
                                        //        break;
                                        //    case 2578:
                                        //        str = Constants.error_2578;
                                        //        break;
                                        //    case 2579:
                                        //        str = Constants.error_2579;
                                        //        break;
                                        //    case 2583:
                                        //        str = Constants.error_2583;
                                        //        break;
                                        //    case 2580:
                                        //        str = Constants.error_2580;
                                        //        break;
                                        //    case 2584:
                                        //        str = Constants.error_2584;
                                        //        break;
                                        //    case 2585:
                                        //        str = Constants.error_2585;
                                        //        break;
                                        //    case 2586:
                                        //        str = Constants.error_2586;
                                        //        break;
                                        //    case 2587:
                                        //        str = Constants.error_2587;
                                        //        break;
                                        //    case 2591:
                                        //        str = Constants.error_2591;
                                        //        break;
                                        //    case 2592:
                                        //        str = Constants.error_2592;
                                        //        break;
                                        //    case 2593:
                                        //        str = Constants.error_2593;
                                        //        break;
                                        //    case 2594:
                                        //        str = Constants.error_2594;
                                        //        break;
                                        //    case 2595:
                                        //        str = Constants.error_2595;
                                        //        break;
                                        //    case 2596:
                                        //        str = Constants.error_2596;
                                        //        break;
                                        //    case 2597:
                                        //        str = Constants.error_2597;
                                        //        break;
                                        //    case 2598:
                                        //        str = Constants.error_2598;
                                        //        break;
                                        //    case 10010:
                                        //        str = Constants.error_10010;
                                        //        break;
                                        //    case 9011:
                                        //        str = Constants.error_9011;
                                        //        break;
                                        //    case 9033:
                                        //        str = Constants.error_9033;
                                        //        break;
                                        //    case 9036:
                                        //        str = Constants.error_9036;
                                        //        break;
                                        //    case 9049:
                                        //        str = Constants.error_9049;
                                        //        break;
                                        //    case 9050:
                                        //        str = Constants.error_9050;
                                        //        break;
                                        //    case 9053:
                                        //        str = Constants.error_9053;
                                        //        break;
                                        //    case 9556:
                                        //        str = Constants.error_9056;
                                        //        break;
                                        //    case 9059:
                                        //        str = Constants.error_9059;
                                        //        break;
                                        //    case 10006:
                                        //        str = Constants.error_10006;
                                        //        break;
                                        //    case 10007:
                                        //        str = Constants.error_10007;
                                        //        break;
                                        //    case 1460:
                                        //        str = Constants.error_1460;
                                        //        break;
                                        //    case 9998:
                                        //        str = Constants.error_9998;
                                        //        break;

                                        //    case 100002:
                                        //        str = Constants.error_100002;
                                        //        break;
                                        //    case 100004:
                                        //        str = Constants.error_100004;
                                        //        break;
                                        //    case 100005:
                                        //        str = Constants.error_100005;
                                        //        break;
                                        //    case 100006:
                                        //        str = Constants.error_100006;
                                        //        break;
                                        //    case 100007:
                                        //        str = Constants.error_100007;
                                        //        break;
                                        //    case 200001:
                                        //        str = Constants.error_200001;
                                        //        break;
                                        //    case 200002:
                                        //        str = Constants.error_200002;
                                        //        break;
                                        //    case 300001:
                                        //        str = Constants.error_300001;
                                        //        break;
                                        //    case 300002:
                                        //        str = Constants.error_300002;
                                        //        break;
                                        //    case 202002:
                                        //        str = Constants.error_202002;
                                        //        break;
                                        //    case 301001:
                                        //        str = Constants.error_301001;
                                        //        break;
                                        //    case 301013:
                                        //        str = Constants.error_301013;
                                        //        break;
                                        //    case 301014:
                                        //        str = Constants.error_301014;
                                        //        break;
                                        //    case 301019:
                                        //        str = Constants.error_301019;
                                        //        break;
                                        //    case 301023:
                                        //        str = Constants.error_301023;
                                        //        break;
                                        //    case 302003:
                                        //        str = Constants.error_302003;
                                        //        break;
                                        //    case 302010:
                                        //        str = Constants.error_301013;
                                        //        break;
                                        //    case 302011:
                                        //        str = Constants.error_302011;
                                        //        break;
                                        //    case 302012:
                                        //        str = Constants.error_302012;
                                        //        break;
                                        //    case 302018:
                                        //        str = Constants.error_302018;
                                        //        break;
                                        //    case 302020:
                                        //        str = Constants.error_302020;
                                        //        break;
                                        //    case 302030:
                                        //        str = Constants.error_302018;
                                        //        break;
                                        //    case 302064:
                                        //        str = Constants.error_302018;
                                        //        break;
                                        //    case 302065:
                                        //        str = Constants.error_302018;
                                        //        break;
                                        //    case 302072:
                                        //        str = Constants.error_302072;
                                        //        break;
                                        //    case 302074:
                                        //        str = Constants.error_302074;
                                        //        break;
                                        //    case 302079:
                                        //        str = Constants.error_302079;
                                        //        break;
                                        //    case 302080:
                                        //        str = Constants.error_302080;
                                        //        break;
                                        //    case 302082:
                                        //        str = Constants.error_302082;
                                        //        break;
                                        //    case 302110:
                                        //        str = Constants.error_302110;
                                        //        break;
                                        //    case 302111:
                                        //        str = Constants.error_302111;
                                        //        break;
                                        //    case 303002:
                                        //        str = Constants.error_303002;
                                        //        break;
                                        //    case 303023:
                                        //        str = Constants.error_303023;
                                        //        break;
                                        //    case 303024:
                                        //        str = Constants.error_303024;
                                        //        break;
                                        //    case 303031:
                                        //        str = Constants.error_303031;
                                        //        break;
                                        //    case 303032:
                                        //        str = Constants.error_303032;
                                        //        break;
                                        //    case 303043:
                                        //        str = Constants.error_303043;
                                        //        break;
                                        //    case 309116:
                                        //        str = Constants.error_309116;
                                        //        break;
                                        //    case 309118:
                                        //        str = Constants.error_309118;
                                        //        break;
                                        //    case 310001:
                                        //        str = Constants.error_310001;
                                        //        break;
                                        //    case 312005:
                                        //        str = Constants.error_312005;
                                        //        break;
                                        //    case 312008:
                                        //        str = Constants.error_312008;
                                        //        break;
                                        //    case 3100011:
                                        //        str = Constants.error_3100011;
                                        //        break;
                                        //    case 312017:
                                        //        str = Constants.error_312017;
                                        //        break;
                                        //    case 8888:
                                        //        str = Constants.error_8888;
                                        //        break;
                                        //    case 9999:
                                        //        str = Constants.error_9999;
                                        //        break;
                                        //    case 302073:
                                        //        str = Constants.error_302073;
                                        //        break;
                                        //    case 1101:
                                        //        str = Constants.error_1101;
                                        //        break;
                                        //    case 1102:
                                        //        str = Constants.error_1102;
                                        //        break;
                                        //    case 1103:
                                        //        str = Constants.error_1103;
                                        //        break;
                                        //    case 1104:
                                        //        str = Constants.error_1104;
                                        //        break;
                                        //    case 1107:
                                        //        str = Constants.error_1107;
                                        //        break;
                                        //    case 1108:
                                        //        str = Constants.error_1108;
                                        //        break;
                                        //    case 11105:
                                        //        str = Constants.error_11105;
                                        //        break;
                                        //    case 11106:
                                        //        str = Constants.error_11106;
                                        //        break;
                                        //    case 0005:
                                        //        str = Constants.error_0005;
                                        //        break;

                                        //    case 13210:
                                        //        str = Constants.error_13210;
                                        //        break;
                                        //    case 13211:
                                        //        str = Constants.error_13211;
                                        //        break;
                                        //    case 13212:
                                        //        str = Constants.error_13212;
                                        //        break;
                                        //    case 13213:
                                        //        str = Constants.error_13213;
                                        //        break;
                                        //    case 13215:
                                        //        str = Constants.error_13215;
                                        //        break;
                                        //    case 13216:
                                        //        str = Constants.error_13216;
                                        //        break;
                                        //    case 13217:
                                        //        str = Constants.error_13217;
                                        //        break;
                                        //    case 13218:
                                        //        str = Constants.error_13218;
                                        //        break;
                                        //    case 13219:
                                        //        str = Constants.error_13219;
                                        //        break;
                                        //    case 13220:
                                        //        str = Constants.error_13220;
                                        //        break;
                                        //    case 13221:
                                        //        str = Constants.error_13221;
                                        //        break;
                                        //    case 13222:
                                        //        str = Constants.error_13222;
                                        //        break;
                                        //    case 13223:
                                        //        str = Constants.error_13223;
                                        //        break;
                                        //    case 13224:
                                        //        str = Constants.error_13224;
                                        //        break;
                                        //    case 13225:
                                        //        str = Constants.error_13225;
                                        //        break;
                                        //    case 13226:
                                        //        str = Constants.error_13226;
                                        //        break;
                                        //    case 13227:
                                        //        str = Constants.error_13227;
                                        //        break;
                                        //    case 13228:
                                        //        str = Constants.error_13228;
                                        //        break;
                                        //    case 13229:
                                        //        str = Constants.error_13229;
                                        //        break;
                                        //    case 1001:
                                        //        str = Constants.error_1001;
                                        //        break;
                                        //    case 1002:
                                        //        str = Constants.error_1002;
                                        //        break;
                                        //    case 1003:
                                        //        str = Constants.error_1003;
                                        //        break;
                                        //    case 1004:
                                        //        str = Constants.error_1004;
                                        //        break;
                                        //}
                                    }
                                }
                            }
                        }
                    }
                }
            }
            return flag ? str : msg;
        }
    }
}