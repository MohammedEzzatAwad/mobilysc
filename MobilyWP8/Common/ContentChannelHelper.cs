﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using MobilyWP8.VOs;

namespace MobilyWP8.Common
{
    public interface IContentChannelHelper
    {
        ContentChannel GetContentChannel();
        ContentChannel.ContentData.Content GetContentChannelData(string msg);
    }

    public class ContentChannelHelper : IContentChannelHelper
    {
        public ContentChannel GetContentChannel()
        {
            var objContent = new ContentChannel();
            try
            {
                var xDoc = XDocument.Load("Assets\\LocalData\\ContentChannel.xml");
                objContent = (from ContentData in xDoc.Elements("CONTENT")
                              select new ContentChannel
                                         {
                                             ContentChannelAr = (from ContentAr in ContentData.Elements("Category")
                                                                 select new ContentChannel.ContentData
                                                                 {
                                                                     ContentId = ContentAr.Elements("ContentId").Any()? ContentAr.Element("ContentId").Value.Trim() : string.Empty,
                                                                     CategoryName = ContentAr.Elements("CategoryNameAr").Any()? ContentAr.Element("CategoryNameAr").Value.Trim() : string.Empty,
                                                                     Link = ContentAr.Elements("LinkAr").Any() ? ContentAr.Element("LinkAr").Value.Replace("#", "&").Trim() : string.Empty,
                                                                     ContentType = ContentAr.Elements("ContentType").Any()? ContentAr.Element("ContentType").Value : string.Empty
                                                                 }).ToList<ContentChannel.ContentData>(),
                                             ContentChannelEn = (from ContentEn in ContentData.Elements("Category")
                                                                 select new ContentChannel.ContentData
                                                                 {
                                                                     ContentId = ContentEn.Elements("ContentId").Any()? ContentEn.Element("ContentId").Value.Trim() : string.Empty,
                                                                     CategoryName = ContentEn.Elements("CategoryNameEn").Any()? ContentEn.Element("CategoryNameEn").Value.Trim() : string.Empty,
                                                                     Link = ContentEn.Elements("LinkEn").Any()? ContentEn.Element("LinkEn").Value.Replace("#", "&").Trim() : string.Empty,
                                                                     ContentType = ContentEn.Elements("ContentType").Any()? ContentEn.Element("ContentType").Value : string.Empty
                                                                 }).ToList<ContentChannel.ContentData>(),
                                         }).SingleOrDefault<ContentChannel>();
                return objContent;
            }
            catch (Exception ex) { throw; }
        }

        public ContentChannel.ContentData.Content GetContentChannelData(string msg)
        {
            var content = new ContentChannel.ContentData.Content();
            try
            {
                var xDoc = XElement.Parse(msg);
                content.UpdateDate = xDoc.FirstAttribute.Value;

                foreach (XElement xelement2 in xDoc.Descendants())
                {
                    if (xelement2.Name == (XName) Constants.AlhawaServices_ContentChannel_Type)
                        content.Type = xelement2.Value;
                    else if (xelement2.Name == Constants.AlhawaServices_ContentChannel_Channel_Code)
                        content.ChannelCode = xelement2.Value;
                    else if (xelement2.Name == Constants.AlhawaServices_ContentChannel_Provider_Id)
                        content.ProviderId = xelement2.Value;
                    else if (xelement2.Name == Constants.AlhawaServices_ContentChannel_Language_Id)
                        content.LanguageId = xelement2.Value;
                    else if (xelement2.Name == Constants.AlhawaServices_ContentChannel_body)
                        content.Body = xelement2.Value;
                }
                return content;
            }
            catch (Exception ex) { throw; }
        }
    }
}
