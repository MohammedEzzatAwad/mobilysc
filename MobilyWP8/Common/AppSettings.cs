﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO.IsolatedStorage;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MobilyWP8.Common
{
    public class AppSettings
    {
        // Isolated storage settings
        readonly IsolatedStorageSettings _isolatedStore;

        // The isolated storage key names of the settings
        public string IsLoginSettingKeyName = "IsLoginSetting";

        // The default value of the settings
        const bool IsLoginSettingDefault = false;

        /// <summary>
        /// Constructor that gets the application settings.
        /// </summary>
        public AppSettings()
        {
            try
            {
                // Get the settings for this application.
                _isolatedStore = IsolatedStorageSettings.ApplicationSettings;
            }
            catch (Exception ex)
            {
                Debug.WriteLine("Exception while using IsolatedStorageSettings: " + ex);
            }
        }

        /// <summary>
        /// Update a setting value for our application. If the setting does not exist, then add the setting.
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public bool AddOrUpdateValue(string key, Object value)
        {
            var valueChanged = false;

            // If the key exists
            if (_isolatedStore.Contains(key))
            {
                // If the value has changed
                if (_isolatedStore[key] != value)
                {
                    // Store the new value
                    _isolatedStore[key] = value;
                    valueChanged = true;
                }
            }
            // Otherwise create the key.
            else
            {
                _isolatedStore.Add(key, value);
                valueChanged = true;
            }
            return valueChanged;
        }

        /// <summary>
        /// Get the current value of the setting, or if it is not found, set the 
        /// setting to the default setting.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key"></param>
        /// <param name="defaultValue"></param>
        /// <returns></returns>
        public T GetValueOrDefault<T>(string key, T defaultValue)
        {
            T value;

            // If the key exists, retrieve the value.
            if (_isolatedStore.Contains(key))
            {
                value = (T)_isolatedStore[key];
            }
            // Otherwise, use the default value.
            else
            {
                value = defaultValue;
            }
            return value;
        }

        /// <summary>
        /// Save the settings.
        /// </summary>
        public void Save()
        {
            _isolatedStore.Save();
        }

        /// <summary>
        /// Property to get and set a IsLogin Setting Key.
        /// </summary>
        public bool IsLoginSetting
        {
            get
            {
                return GetValueOrDefault<bool>(IsLoginSettingKeyName, IsLoginSettingDefault);
            }
            set
            {
                AddOrUpdateValue(IsLoginSettingKeyName, value);
                Save();
            }
        }
    }
}
