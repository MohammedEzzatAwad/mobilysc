﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace MobilyWP8.Common
{
    public class Plans
    {
        #region"Variables"
        public List<Plan> ListOfPlans { get; set; }
        public List<PlanE> ListOfPlansEn { get; set; }
        public List<PlanA> ListOfPlansAr { get; set; }
        #endregion

        #region"Sub Classes"
        public class Plan
        {
            #region"Variables"
            public string Id { get; set; }
            public string Title_en { get; set; }
            public string Title_ar { get; set; }
            public string Content_en { get; set; }
            public string Content_ar { get; set; }
            public string Parent { get; set; }
            public string Link { get; set; }
            public string Link_en { get; set; }
            public string Link_ar { get; set; }
            #endregion
        }

        public class PlanE
        {
            #region"Variables"
            public string Id { get; set; }
            public string Title { get; set; }
            public string Content { get; set; }
            public string Parent { get; set; }
            public string LinkName { get; set; }
            public string Link { get; set; }
            #endregion
        }

        public class PlanA
        {
            #region"Variables"
            public string Id { get; set; }
            public string Title { get; set; }
            public string Content { get; set; }
            public string Parent { get; set; }
            public string LinkName { get; set; }
            public string Link { get; set; }
            #endregion
        }
        #endregion
    }

    public interface IPlansDataHelper
    {
        Plans GetPlansData();
    }

    public class PlansDataHelper : IPlansDataHelper
    {
        public Plans GetPlansData()
        {
            try
            {
                Plans objData = new Plans();

                XDocument xDoc = XDocument.Load("Assets\\LocalData\\PlansData.xml");

                objData = (from XMLDATA in xDoc.Elements("MOBILYPLANS")
                           select new Plans
                           {
                               ListOfPlans = (from PLANS in XMLDATA.Elements("Plans")
                                              select new Plans.Plan
                                              {
                                                  Id = PLANS.Elements("id").Any() ? PLANS.Element("id").Value.ToString() : string.Empty,
                                                  Title_ar = PLANS.Elements("title_ar").Any() ? PLANS.Element("title_ar").Value.ToString() : string.Empty,
                                                  Title_en = PLANS.Elements("title_en").Any() ? PLANS.Element("title_en").Value.ToString() : string.Empty,
                                                  Content_ar = PLANS.Elements("content_ar").Any() ? PLANS.Element("content_ar").Value.ToString() : string.Empty,
                                                  Content_en = PLANS.Elements("content_en").Any() ? PLANS.Element("content_en").Value.ToString() : string.Empty,
                                                  Link = PLANS.Elements("link").Any() ? PLANS.Element("link").Value.ToString() : string.Empty,
                                                  Parent = PLANS.Elements("parent").Any() ? PLANS.Element("parent").Value.ToString() : string.Empty,
                                                  Link_ar = PLANS.Elements("link_ar").Any() ? PLANS.Element("link_ar").Value.ToString() : string.Empty,
                                                  Link_en = PLANS.Elements("link_en").Any() ? PLANS.Element("link_en").Value.ToString() : string.Empty,
                                              }).ToList(),
                               ListOfPlansAr = (from PLANS in XMLDATA.Elements("Plans")
                                                select new Plans.PlanA
                                                {
                                                    Id = PLANS.Elements("id").Any() ? PLANS.Element("id").Value.ToString() : string.Empty,
                                                    Title = PLANS.Elements("title_ar").Any() ? PLANS.Element("title_ar").Value.ToString() : string.Empty,
                                                    Content = PLANS.Elements("content_ar").Any() ? PLANS.Element("content_ar").Value.ToString() : string.Empty,
                                                    LinkName = PLANS.Elements("link").Any() ? PLANS.Element("link").Value.ToString() : string.Empty,
                                                    Parent = PLANS.Elements("parent").Any() ? PLANS.Element("parent").Value.ToString() : string.Empty,
                                                    Link = PLANS.Elements("link_ar").Any() ? PLANS.Element("link_ar").Value.ToString() : string.Empty,

                                                }).ToList(),
                               ListOfPlansEn = (from PLANS in XMLDATA.Elements("Plans")
                                                select new Plans.PlanE
                                                {
                                                    Id = PLANS.Elements("id").Any() ? PLANS.Element("id").Value.ToString() : string.Empty,
                                                    Title = PLANS.Elements("title_en").Any() ? PLANS.Element("title_en").Value.ToString() : string.Empty,
                                                    Content = PLANS.Elements("content_en").Any() ? PLANS.Element("content_en").Value.ToString() : string.Empty,
                                                    LinkName = PLANS.Elements("link").Any() ? PLANS.Element("link").Value.ToString() : string.Empty,
                                                    Parent = PLANS.Elements("parent").Any() ? PLANS.Element("parent").Value.ToString() : string.Empty,
                                                    Link = PLANS.Elements("link_en").Any() ? PLANS.Element("link_en").Value.ToString() : string.Empty,

                                                }).ToList()
                           }).SingleOrDefault<Plans>();

                return objData;
            }
            catch { throw; }
        }
    }
}
