﻿namespace MobilyWP8.Common
{
    public static class Constants
    {
        public static string request_url = "https://www.mobily.com.sa/IPhoneService/mobilyIphoneService.do";
        public static string login_MOBILY_IPHONE_REQUEST = "MOBILY_IPHONE_REQUEST";
        public static string login_FunctionId = "FunctionId";
        public static string login_device = "device";
        public static string login_UserName = "UserName";
        public static string login_Password = "Password";
        public static string login_RequestorLanguage = "RequestorLanguage";
        public static string login_device_number = "5";
        public static string login_LOGIN = "LOGIN";
        public static string login_LOGOUT = "LOGOUT";
        public static string login_EA = "E/A";
        public static string login_MOBILY_IPHONE_REPLY = "MOBILY_IPHONE_REPLY";
        public static string login_HashCode = "HashCode";
        public static string login_root = "<root>";
        public static string login_rootEnd = "</root>";
        public static string login_ERROR = "ERROR";
        public static string login_ErrorCode = "ErrorCode";
        public static string balancebill_GET_BALANCE = "GET_BALANCE";
        public static string balancebill_GET_BILL_INFO = "GET_BILL_INFO";
        public static string balancebill_GET_BILL_SUMMARY = "GET_BILL_SUMMARY";
        public static string balancebill_GET_BILL_PERSONAL_INFO = "GET_BILL_PERSONAL_INFO";
        public static string balance_E = "E";
        public static string balance_A = "A";
        public static string balancebill_LINENUMBER = "LINE_NUMBER";
        public static string balancebill_UnbilledAmount = "UnbilledAmount";
        public static string balancebill_DueAmount = "DueAmount";
        public static string balancebill_FreeMinutes = "FreeMinutes";
        public static string balancebill_FreeOnNetMinutes = "FreeOnNetMinutes";
        public static string balancebill_FreeSMS = "FreeSMS";
        public static string balancebill_FreeOnNetSMS = "FreeOnNetSMS";
        public static string balancebill_FreeMMS = "FreeMMS";
        public static string balancebill_FreeOnNetMMS = "FreeOnNetMMS";
        public static string balancebill_FreeGPRS = "FreeGPRS";
        public static string balancebill_NationalFavoriteNumber = "NationalFavoriteNumber";
        public static string balancebill_InternationalFavoriteNumber = "InternationalFavoriteNumber";
        public static string balancebill_EXPIRATION_DATE = "EXPIRATION_DATE";
        public static string balancebill_BALANCE = "BALANCE";
        public static string balancebill_BILL_PERSONAL_INFO = "BILL_PERSONAL_INFO";
        public static string balancebill_Name = "Name";
        public static string balancebill_BillNumber = "BillNumber";
        public static string balancebill_POBox = "P.O.Box";
        public static string balancebill_Address = "Address";
        public static string balancebill_City = "City";
        public static string balancebill_Country = "Country";
        public static string balancebill_StartDate = "StartDate";
        public static string balancebill_EndDate = "EndDate";
        public static string balancebill_Tarrif_Plan = "Tarrif_Plan";
        public static string balancebill_Due_Date = "Due_Date";
        public static string balancebill_Previous_Amount = "Previous_Amount";
        public static string balancebill_MonthlyFee = "MonthlyFee";
        public static string balancebill_AdditionalFee = "AdditionalFee";
        public static string balancebill_UsageAmount = "UsageAmount";
        public static string balancebill_Discount = "Discount";
        public static string balancebill_PaidAmount = "PaidAmount";
        public static string balancebill_AmountDue = "AmountDue";
        public static string balancebill_BILL_SUMMARY = "BILL_SUMMARY";
        public static string balancebill_BILL_INFO = "BILL_INFO";

        public static string CREDIT_TRANSFER = "CREDIT_TRANSFER";
        public static string RecipientMSISDN = "RecipientMSISDN";
        public static string TransferredAmount = "TransferredAmount";
        public static string ConfirmationKey = "ConfirmationKey";


        public static string BILL_METHOD_FunctionId = "Email_Invoice_Inquiry";
        public static string BILL_METHOD_InvoiceMechanism = "InvoiceMechanism";
        public static string BILL_METHOD_Email = "Email";
        public static string BILL_METHOD_Email_Invoice = "Email_Invoice";

        public static string neqaty_LOYALTY_VIEW_SUMMARY = "LOYALTY_VIEW_SUMMARY";
        public static string neqaty_LOYALTY_INQUIRY_ITEMS = "LOYALTY_INQUIRY_ITEMS";
        public static string neqaty_CurrentBalance = "CurrentBalance";
        public static string neqaty_EarnedPoints = "EarnedPoints";
        public static string neqaty_RedeemedPoints = "RedeemedPoints";
        public static string neqaty_ExpirySchedulerPoints = "ExpirySchedulerPoints";
        public static string neqaty_LostPoints = "LostPoints";
        public static string neqaty_LOYALTY_REDEMPTION = "LOYALTY_REDEMPTION";
        public static string neqaty_ItemCode = "ItemCode";


        public static string SUPPLEMENTARY_SERVICES_INQUIRY = "SUPPLEMENTARY_SERVICES_INQUIRY";
        public static string ListOfSupplementaryService = "ListOfSupplementaryService";
        public static string SupplementaryService = "SupplementaryService";
        public static string SupplementaryService_ServiceName = "ServiceName";
        public static string SupplementaryService_Status = "Status";
        public static string SupplementaryService_SubscriptionFee = "SubscriptionFee";
        public static string SupplementaryService_MonthlyFee = "SupplementaryService";

        public static string SupplementaryService_SUPPLEMENTARY_SERVICE = "SUPPLEMENTARY_SERVICE";
        public static string SupplementaryService_Action = "Action";
        public static string SupplementaryService_ServiceType = "ServiceType";
        


        public static string AlhawaServices_Alhawa_List = "3alhawa_List";
        public static string AlhawaServices_CHANNELS = "CHANNELS";
        public static string AlhawaServices_CHANNEL = "CHANNEL";

        public static string AlhawaServices_CHANNEL_ID = "CHANNEL_ID";
        public static string AlhawaServices_CHANNEL_NAME_EN = "CHANNEL_NAME_EN";
        public static string AlhawaServices_CHANNEL_NAME_AR = "CHANNEL_NAME_AR";
        public static string AlhawaServices_CHANNEL_IMG = "CHANNEL_IMG";


        public static string AlhawaServices_ContentChannel_Provider_Id = "Provider_Id";
        public static string AlhawaServices_ContentChannel_Channel_Code = "Channel_Code";
        public static string AlhawaServices_ContentChannel_Content_Id = "Content_Id";
        public static string AlhawaServices_ContentChannel_Type = "Type";
        public static string AlhawaServices_ContentChannel_Language_Id = "Language_Id";
        public static string AlhawaServices_ContentChannel_body = "body";


        public static string GET_LATEST_NEWS_PROMOTION = "GET_LATEST_NEWS_PROMOTION";
        public static string NewsPromotion_Id = "Id";
        public static string NewsPromotion_ID = "ID";
        public static string NewsPromotion_Type = "Type";
        public static string NewsPromotion_ItemList = "ItemList";
        public static string NewsPromotion_Item = "Item";
        public static string NewsPromotion_ShortDescEn = "ShortDescEn";
        public static string NewsPromotion_ShortDescAr = "ShortDescAr";
        public static string NewsPromotion_DescEn = "DescEn";
        public static string NewsPromotion_DescAr = "DescAr";
        public static string NewsPromotion_Image = "Image";
        public static string NewsPromotion_ImageFormat = "ImageFormat";

        public static string error_0 = "Your request was submitted successfully. You will be notified once your request is Processed.";
        public static string error_10000 = "Sorry, You are not authorized to view this page.";
        public static string error_10001 = "Sorry, an unknown error has occurred. Please try again later.";
        public static string error_10002 = "Sorry, you cannot login to this Windows 8 application with the provided username and password. This application is intended for consumer accounts and not corporate accounts.";
        public static string error_10005 = "Sorry, you cannot login to this Windows 8 application with the provided username and password. This application is intended for GSM mobile accounts and not Broadband@Home accounts.";
        public static string error_10003 = "Sorry, either the username or the password you provided is not valid.";
        public static string error_10004 = "Sorry, you are not login in yet. Please login first.";
        public static string error_5001 = "Sorry, but prepaid customers do not have bills.";
        public static string error_5002 = "Currently, there is no bill under your account in the system. Please check back after the next billing cycle to view your bills.";
        public static string error_10191 = "Sorry, but you don't have enough points.";
		public static string error_10008 = "Sorry, the request is invalid.";
		public static string error_100009 = "Sorry, your account is yet to be activated. You were sent a confirmation e-mail to your {xxxxxxxx} address to activate your account.";
		public static string error_1320 = "Dear customer, this line is currently blocked.";
		public static string error_1315 = "Sorry, but you don’t have enough credit.";
		public static string error_1321 = "Sorry, but you don’t have enough credit.";
		public static string error_1314 = "Sorry, but you already have a pending request.";
		public static string error_1313 = "Sorry, but you have a pending request for the same function.";
		public static string error_1322 = "Sorry, but you are already subscribed to this service.";
		public static string error_1323 = "Sorry, but you are already unsubscribed to this service.";
		public static string error_8103 = "Sorry, but you are already subscribed to this service.";
		public static string error_8104 = "Sorry, but you are already unsubscribed to this service.";
		public static string error_8125 = "Dear customer, this line is currently blocked.";
		public static string error_8129 = "Sorry, but you don’t have enough credit.";
		public static string error_8122 = "Sorry, but you already have a pending request.";
		public static string error_4305 = "Sorry, but you don’t have ";
		public static string error_4304 = "Dear customer, this line is currently blocked.";
		public static string error_9751 = "Sorry, but you already have a pending request.";
		public static string error_12060 = "Sorry, but you don’t have enough credit.";
		public static string error_12054 = "Sorry, but you are already subscribed to a related service.";
		public static string error_12052 = "Sorry, but your package is not eligible for this service.";
		public static string error_4306 = "Sorry, but you already have a pending request.";
		public static string error_1325 = "Sorry, but your package is not eligible for this service.";
		public static string error_12055 = "Sorry, but you are already subscribed to this service.";
		public static string error_1 = "Dear customer, this line is currently blocked.";
		public static string error_2 = "Sorry, but the service is not enabled for you.";
		public static string error_3 = "Sorry, but your subscription had expired.";
		public static string error_4 = "Sorry, but you are already subscribed to one of the packages.";
		public static string error_6 = "Sorry, but your package is not eligible for this service.";
		public static string error_11 = "Sorry, but you already have a pending request.";
		public static string error_12 = "Sorry, but you are already subscribed to this service.";
		public static string error_13 = "Sorry, but this promotion has not started.";
		public static string error_25 = "Sorry, but you are already subscribed to this service. ";
		public static string error_28 = "Sorry, but this number is not eligible for the requested task.";
		public static string error_36 = "Sorry, but the refunding period has ended.";
		public static string error_38 = "Sorry, but you can’t claim a refund after usage.";
		public static string error_8123 = "Sorry, but you already have a pending request.";
		public static string error_6000 = "Sorry, but the mobile number you selected is not owned by you. Please go back to select a mobile number that you own.";
	 
		public static string error_10193 = "Sorry, your line is OG barred. You can only receive calls.";
		 
		public static string error_10195 = "Sorry, but this line is fully barred.";
		public static string error_10196 = "Sorry, but this number is black listed.";
		public static string error_10197 = "Sorry, but your package is not eligible for redemption.";
		public static string error_10203 = "Sorry, but you have exceeded the allowed number of redemptions. Please try later.";
		public static string error_7000 = "Dear customer, the summary you are trying to view is not available now, please try again later.";
		public static string error_7001 = "Sorry, but no items are available now, please try again later.";
		public static string error_10194 = "Dear customer, this line is currently blocked.";
 
		public static string error_1008 = "Sorry, but you can transfer credit to prepaid numbers only.";
		public static string error_10009 = "Sorry, but the activation code you entered is not valid, please try again.";
		public static string error_1900 = "Please try again.";
		public static string error_2551 = "The amount should not have any decimal values.";
		public static string error_2552 = "The daily amount should be less than or equal to 100 SAR.";
		public static string error_2553 = "The amount should be less than or equal to 20 SAR per transaction.";
		public static string error_2554 = "The amount should be greater than 5 SAR per transaction.";
		public static string error_2555 = "The maximum number of transactions per day is 5.";
		public static string error_2556 = "The amount should be in multiples of 5 (5, 10, 15, or 20).";
		public static string error_2557 = "Sorry, but your number is not available.";
		public static string error_2558 = "Sorry but your number is expired.";
		public static string error_2559 = "Sorry, but your package is not a prepaid package.";
		public static string error_2560 = "Sorry but your balance is not enough.";
		public static string error_2561 = "Sorry, but the recipient number is not available.";
		public static string error_2562 = "Sorry, but the recipient numbers is expired.";
		public static string error_2563 = "Sorry, but the recipient package is not a prepaid one.";
		public static string error_2564 = "Sorry, but there was an error in the debit process.";
		public static string error_2565 = "Sorry, but a credit error has occurred.";
		public static string error_2566 = "Sorry, but you can’t make transfers to the same number.";
		public static string error_2567 = "Sorry, but the transfer was not completed successfully, please try again.";
		public static string error_2568 = "Sorry, but the transfer was not completed successfully, please try again.";
		public static string error_2569 = "Sorry, but it was not possible to make a transfer from this mobile number.";
		public static string error_2570 = "Sorry, but the transfer was not completed successfully, please try again.";
		public static string error_2571 = "Sorry, but the transfer was not completed successfully, please try again.";
		public static string error_2572 = "Sorry, but you have exceeded the allowed daily transfers.";
		public static string error_2573 = "Sorry, but your transfer history is not available now, please try again later.";
		public static string error_2574 = "Sorry, but it is not possible to make a transfer from this mobile number.";
		public static string error_2575	= "Sorry, but it is not possible to make a transfer from this mobile number.";
		public static string error_2576 = "Sorry, but the transfer was not completed successfully, please try again.";
		public static string error_2577 = "Sorry, but the transfer was not completed successfully, please try again.";
		public static string error_2578 = "Sorry, but your package is not eligible for this service.";
		public static string error_2579 = "Sorry, but you have exceeded the weekly allowed limit.";
		public static string error_2580 = "Sorry, but you have exceeded the monthly allowed limit.";
		public static string error_2583 = "Sorry, but you are not subscribed to the credit transfer service.";
		public static string error_2584 = "Sorry, but the recipient number is not subscribed to the credit transfer service.";
		public static string error_2585 = "Sorry, but the recipient has reached the limit of daily allowed amount.";
		public static string error_2586 = "Sorry, but the recipient has reached the limit of allowed per week.";
		public static string error_2587 = "Sorry, but the recipient has reached the limit of allowed amount per month.";
		public static string error_2591 = "Sorry, but you are not found in our customer database.";
		public static string error_2592 = "Sorry, but you are still in the grace period.";
		public static string error_2593 = "Sorry, but your package is not eligible for credit transfer service.";
		public static string error_2594 = "Sorry, but the credit transfer service is not available now.";
		public static string error_2595 = "Sorry, but you have an un-paid bill.";
		public static string error_2596 = "Sorry, but you don’t have enough credit limit.";
		public static string error_2597 = "Sorry, but there was timeout to get your info";
		public static string error_2598 = "Sorry, but there was a timeout to get your credit limit.";
		public static string error_10010 = "Sorry, but this is not a valid number.";
		public static string error_9011 = "Sorry, your request could not be processed due to another process in action.";
		public static string error_9033 = "Sorry, you are already subscribed to this service.";
		public static string error_9036 = "Sorry, your request could not be processed due to another process in action.";
		public static string error_9049 = "Sorry, your subscription to this service was not accepted due to insufficient credit.";
		public static string error_9050 = "Sorry, you are not subscribed to any channel.";
		public static string error_9053 = "Sorry, you already have a request in the system to subscribe to this channel.  Please wait until the system processes your request.";
		public static string error_9056 = "Sorry, your package is not eligible for this request";
		public static string error_9059 = "Sorry, your subscription to this service was not accepted due to insufficient credit.";
		public static string error_10006 = "Sorry, either you are not a Mobily user, or you may not have entered the number correctly. Please make sure to enter the correct Mobily number.";
		public static string error_10007 = "Sorry, the PIN code you entered is not correct. Please make sure that you have entered correct PIN code you received.";
		public static string error_1460 = "Sorry, you are not subscribed in ranan service.";
		public static string error_9998 = "Sorry, the file cannot be found for playing.";
		 
		public static string error_000001 = "Successfully accepted. The ordering request has been received successfully, but processing is not completed.";
		public static string error_000003 = "The request for downloading is accepted, and the system will download an RBT after service register (in the non-real-time register).";
		public static string error_100002 = "Sorry, an unknown error has occurred. Please try again later.";
		public static string error_100004 = "Sorry, an unknown error has occurred. Please try again later.";
		public static string error_100005 = "Sorry, an unknown error has occurred. Please try again later.";
		public static string error_100006 = "Sorry, an unknown error has occurred. Please try again later.";
		public static string error_100007 = "The service does not exist currently. Please try again later.";
		public static string error_200001 = "Sorry, an unknown error has occurred. Please try again later.";
		public static string error_200002 = "Invalid mobile number format.";
		public static string error_202002 = "Sorry, an unknown error has occurred. Please try again later.";
		public static string error_300001 = "Sorry, an unknown error has occurred. Please try again later.";
		public static string error_300002 = "Sorry, an unknown error has occurred. Please try again later.";
		public static string error_301001 = "The subscriber does not exist.";
		public static string error_301013 = "The subscriber is not allowed to download RBTs due to his service state.";
		public static string error_301014 = "Sorry, you are not allowed due to the line or service status.";
		public static string error_301019 = "Sorry, an unknown error has occurred. Please try again later.";
		public static string error_301023 = "The consumption limit is exceeded.";
		public static string error_302003 = "This tone does not exist anymore.";
		public static string error_302010 = "Failed to purchase this tone due to Charging issue.";
		public static string error_302011 = "Sorry, this tone was already ordered.";
		public static string error_302012 = "Sorry, an unknown error has occurred. Please try again later.";
		public static string error_302018 = "The tone is not ordered.";
		public static string error_302020 = "Sorry, an unknown error has occurred. Please try again later.";
		public static string error_302030 = "The time for playing the tone is overlapped.";
		public static string error_302064 = "The number of music boxes that can be uploaded reached the maximum.";
		public static string error_302065 = "Sorry, an unknown error has occurred. Please try again later.";
		public static string error_302072 = "The calling  number does not exist";
		public static string error_302074 = "Sorry, an unknown error has occurred. Please try again later.";
		public static string error_302079 = "The calling number is set already.";
		public static string error_302080 = "Sorry, an unknown error has occurred. Please try again later.";
		public static string error_302082 = "Sorry, an unknown error has occurred. Please try again later.";
		public static string error_302110 = "Sorry, an unknown error has occurred. Please try again later.";
		public static string error_302111 = "Sorry, an unknown error has occurred. Please try again later.";
		public static string error_303002 = "The calling group does not exist.";
		public static string error_303023 = "The number of downloaded RBTs reaches the maximum.";
		public static string error_303024 = "The number of assignments reaches the maximum.";
		public static string error_303031 = "The new RBT group exists already.";
		public static string error_303032 = "The RBT group does not exist.";
		public static string error_303043 = "The content (such as a music box) of the RBT group cannot be modified.";
		public static string error_309116 = "The subscriber type and access type are not set.";
		public static string error_309118 = "The subscriber whose phone number is in the number segment is not allowed to register the RBT service.";
		public static string error_310001 = "Sorry, you do not have enough balance to purchase this tone.";
		public static string error_312005 = "Sorry, an unknown error has occurred. Please try again later.";
		public static string error_312008 = "Sorry, an unknown error has occurred. Please try again later.";
		public static string error_3100011 = "Sorry, you do not have enough credit limit to purchase this tone.";
		public static string error_312017 = "Sorry, an unknown error has occurred. Please try again later.";
		public static string error_8888 = "Sorry, an unknown error has occurred. Please try again later.";
		public static string error_9999 = "Sorry, an unknown error has occurred. Please try again later.";
		public static string error_302073 = "The subscriber or tone does not exist.";
		public static string error_1101 = "Sorry, the entered mobile number may be wrong or doesn't exist in the website.";
		public static string error_1102 = "Sorry, an unknown error has occurred. Please try again later.";
		public static string error_1103 = "Sorry, an unknown error has occurred. Please try again later.";
		public static string error_1104 = "Sorry, the request is invalid.";
		public static string error_1107 = "Sorry, an unknown error has occurred. Please try again later.";
		public static string error_1108 = "Sorry, an unknown error has occurred. Please try again later.";
		public static string error_11105 = "Sorry, the entered user name is not registered. ";
		public static string error_11106 = "Sorry, the entered ID number doesn't match your contract information.";
		public static string error_0000 = "An e-mail message was sent to the provided e-mail address to complete the registration process and activate your account. ";
		public static string error_0001 = "Sorry, the provided number is not correct.  Please make sure you typed your number correctly before submitting.";
		public static string error_0002 = "Sorry, the provided national ID or Iqama no is incorrect. Please provide the correct id.";
		public static string error_0003 = "Sorry, the entered activation code is not valid please try again.";
		public static string error_0004 = "Sorry, this user already registered, please login!.";
		public static string error_0005 = "Sorry, an unexpected system error has occurred!. Sorry for the inconvenience. Please try again later. ";
		public static string error_0006 = "Sorry, this user name already exists."; 
		public static string error_13210 = "An error occurred, and your request wasn't carried out successfully. Please try again later.";
		public static string error_13211 = "An error occurred, and your request wasn't carried out successfully. Please try again later.";
		public static string error_13212 = "An error occurred, and your request wasn't carried out successfully. Please try again later.";
		public static string error_13213 = "Sorry, but the voucher number you entered is not valid. Make sure that you have entered the number correctly.";
		public static string error_13215 = "Sorry, but the voucher number you entered is not valid. Make sure that you have entered the number correctly.";
		public static string error_13216 = "Sorry, but the voucher number you entered is not valid. Make sure that you have entered the number correctly.";
		public static string error_13217 = "Sorry, but the voucher number you entered is not valid. Make sure that you have entered the number correctly.";
		public static string error_13218 = "Sorry, but the voucher you are using has expired. Please use a valid voucher card.";
		public static string error_13219 = "Sorry, but the voucher number you entered is not valid. Make sure that you have entered the number correctly.";
		public static string error_13220 = "Sorry, but the voucher number you entered is not valid. Make sure that you have entered the number correctly.";
		public static string error_13221 = "Sorry, but the voucher number you entered is not valid. Make sure that you have entered the number correctly.";
		public static string error_13222 = "Sorry, but the voucher number you entered is not valid. Make sure that you have entered the number correctly.";
		public static string error_13223 = "Sorry, but the voucher number you entered is not valid. Make sure that you have entered the number correctly.";
		public static string error_13224 = "Sorry, but the voucher number you entered is not valid. Make sure that you have entered the number correctly.";
		public static string error_13225 = "An error occurred, and your request wasn't carried out successfully. Please try again later.";
		public static string error_13226 = "An error occurred, and your request wasn't carried out successfully. Please try again later.";
		public static string error_13227 = "Sorry, but the voucher number you entered is not valid. Make sure that you have entered the number correctly.";
		public static string error_13228 = "An error occurred, and your request wasn't carried out successfully. Please try again later.";
		public static string error_13229 = "An error occurred, and your request wasn't carried out successfully. Please try again later.";
		public static string error_1001 = "Sorry, but the voucher number you entered is not valid. Make sure that you have entered the number correctly.";
		public static string error_1002 = "Sorry, but the voucher number you entered is not valid. Make sure that you have entered the number correctly.";
		public static string error_1003 = "An error occurred, and your request wasn't carried out successfully. Please try again later.";
		public static string error_1004 = "An error occurred, and your request wasn't carried out successfully. Please try again later.";


        public static string progress_null = string.Empty;
        public static string progress_Starting = "Starting ...";
        public static string progress_Login = "Login : done ...";
        public static string progress_Balance = "Balance : done ...";
        public static string progress_BillInfo = "Bill informations : done ...";
        public static string progress_BillSummary = "Bill summary : done ...";
        public static string progress_BillPersonalInfo = "Bill personal information : done ...";


        //Billing Method Constants
        public static string PatternEmail = @"^(([^<>()[\]\\.,;:\s@\""]+" + @"(\.[^<>()[\]\\.,;:\s@\""]+)*)|(\"".+\""))@" + @"((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}" + @"\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+" + @"[a-zA-Z]{2,}))$";
        public static string EmailMessage = "After you send your request to update invoice by email, you will receive confirmation email, you must reply to this email to complete your request.";
        public static string PostalMessage = "Your bill will be sent to your postal address. To change this address, please call our call center at 1100 or visit our nearest shop.";

        static Constants()
        {
        }
    }
}