﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Interactivity;
using Microsoft.Phone.Controls;

namespace MobilyWP8.Common
{
    public class TrackablePanoramaBehavior : Behavior<Panorama>
    {
        private Panorama _panarama = null;
        private bool _updatedFromUI = false;

        // DP for binding index
        public static readonly DependencyProperty SelectedIndexProperty =
            DependencyProperty.Register("SelectedIndex", typeof(int), typeof(TrackablePanoramaBehavior),
            new PropertyMetadata(0, new PropertyChangedCallback(SelectedIndexPropertyChanged)));

        // Index changed by view model
        private static void SelectedIndexPropertyChanged(DependencyObject dpObj, DependencyPropertyChangedEventArgs change)
        {
            if(change.NewValue.GetType() != typeof(int) || dpObj.GetType() != typeof(TrackablePanoramaBehavior))
                return;

            TrackablePanoramaBehavior track = (TrackablePanoramaBehavior)dpObj;

            // If this flag is not checked, the panorama smooth transition is overridden
            if (!track._updatedFromUI)
            {
                Panorama pan = track._panarama;

                int index = (int)change.NewValue;

                if (pan.Items.Count > index)
                {
                    pan.DefaultItem = pan.Items[(int)change.NewValue];
                }
            }

            track._updatedFromUI = false;
        }

        public int SelectedIndex
        {
            get { return (int)GetValue(SelectedIndexProperty); }
            set { SetValue(SelectedIndexProperty, value); }
        }

        protected override void OnAttached()
        {
            base.OnAttached();

            this._panarama = base.AssociatedObject as Panorama;
            this._panarama.SelectionChanged += _panarama_SelectionChanged;
        }        

        protected override void OnDetaching()
        {
            base.OnDetaching();

            if(this._panarama != null)
                this._panarama.SelectionChanged += _panarama_SelectionChanged;
        }

        // Index changed by UI
        private void _panarama_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            _updatedFromUI = true;
            SelectedIndex = _panarama.SelectedIndex;
        }
    }
}

