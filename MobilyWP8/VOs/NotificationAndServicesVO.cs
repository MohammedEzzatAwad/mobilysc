﻿using System.Collections.ObjectModel;
using System.Xml.Linq;
using GalaSoft.MvvmLight;
using MobilyWP8.Common;
using MobilyWin8.Helpers;


namespace MobilyWP8.VOs
{
    public class NotificationAndServicesVO : ViewModelBase
    {
        protected ObservableCollection<NewsPromotionVO> _newsColl = (ObservableCollection<NewsPromotionVO>)null;
        protected ObservableCollection<NewsPromotionVO> _promotionColl = (ObservableCollection<NewsPromotionVO>)null;

        private const string NewsPropertyName = "News";
        public   ObservableCollection<NewsPromotionVO> News
        {
            get
            {
                return this._newsColl;
            }
            set
            {  this.Set(NewsPropertyName, ref _newsColl, value);  }
        }

        private const string PromotionsPropertyName = "Promotions";
        public ObservableCollection<NewsPromotionVO> Promotions
        {
            get { return _promotionColl; }
            set { Set(PromotionsPropertyName, ref _promotionColl, value); }
        }


        private int _counts;
        private const string CountsPropertyName = "Counts";
        public int Counts
        {
            get { return _counts; }
            set { Set(CountsPropertyName, ref _counts, value); }
        }

        public void ParseXmlNews(string msg)
        {
            msg = msg.Replace("---!>", "-->");
            foreach (
                XElement xelement1 in XElement.Parse(Constants.login_root + msg + Constants.login_rootEnd).Descendants()
                )
            {
                if (xelement1.Name == (XName)Constants.login_MOBILY_IPHONE_REPLY)
                {
                    foreach (XElement xelement2 in xelement1.Descendants())
                    {
                        if (xelement2.Name == (XName)Constants.NewsPromotion_ItemList)
                        {
                            News = new ObservableCollection<NewsPromotionVO>();

                            foreach (XElement xelement3 in xelement2.Descendants())
                            {
                                if (xelement3.Name == Constants.NewsPromotion_Item)
                                {
                                    NewsPromotionVO item = new NewsPromotionVO();
                                    foreach (XElement xelement4 in xelement3.Descendants())
                                    {
                                        if (xelement4.Name == (XName)Constants.NewsPromotion_ID)
                                            item.Id = xelement4.Value;
                                        else if (xelement4.Name == (XName)Constants.NewsPromotion_ShortDescEn)
                                            item.ShortDescEn = xelement4.Value;
                                        else if (xelement4.Name == (XName)Constants.NewsPromotion_ShortDescAr)
                                            item.ShortDescAr = xelement4.Value;
                                        else if (xelement4.Name == (XName)Constants.NewsPromotion_DescEn)
                                            item.DescEn = xelement4.Value;
                                        else if (xelement4.Name == (XName)Constants.NewsPromotion_DescAr)
                                            item.DescAr = xelement4.Value;
                                        //else if (xelement4.Name == (XName)Constants.NewsPromotion_Image)
                                        //    item.Image = ImageDecodingHelper.ImageFromBase64String(xelement4.Value);
                                        else if (xelement4.Name == (XName)Constants.NewsPromotion_ImageFormat)
                                            item.ImageFormat = xelement4.Value;
                                    }
                                    News.Add(item);
                                    Counts++;
                                }
                            }
                        }
                    }
                }
            }
        }

        public void ParseXmlPromotions(string msg)
        {
            msg = msg.Replace("---!>", "-->");
            foreach (XElement xelement1 in XElement.Parse(Constants.login_root + msg + Constants.login_rootEnd).Descendants())
            {
                if (xelement1.Name == (XName)Constants.login_MOBILY_IPHONE_REPLY)
                {
                    foreach (XElement xelement2 in xelement1.Descendants())
                    {
                        if (xelement2.Name == (XName)Constants.NewsPromotion_ItemList)
                        {
                            Promotions = new ObservableCollection<NewsPromotionVO>();

                            foreach (XElement xelement3 in xelement2.Descendants())
                            {
                                if (xelement3.Name == Constants.NewsPromotion_Item)
                                {
                                    NewsPromotionVO item = new NewsPromotionVO();
                                    foreach (XElement xelement4 in xelement3.Descendants())
                                    {
                                        if (xelement4.Name == (XName)Constants.NewsPromotion_ID)
                                            item.Id = xelement4.Value;
                                        else if (xelement4.Name == (XName)Constants.NewsPromotion_ShortDescEn)
                                            item.ShortDescEn = xelement4.Value;
                                        else if (xelement4.Name == (XName)Constants.NewsPromotion_ShortDescAr)
                                            item.ShortDescAr = xelement4.Value;
                                        else if (xelement4.Name == (XName)Constants.NewsPromotion_DescEn)
                                            item.DescEn = xelement4.Value;
                                        else if (xelement4.Name == (XName)Constants.NewsPromotion_DescAr)
                                            item.DescAr = xelement4.Value;
                                        //else if (xelement4.Name == (XName)Constants.NewsPromotion_Image)
                                        //    item.Image = ImageDecodingHelper.ImageFromBase64String(xelement4.Value);
                                        else if (xelement4.Name == (XName)Constants.NewsPromotion_ImageFormat)
                                            item.ImageFormat = xelement4.Value;
                                    }
                                    Promotions.Add(item);
                                    Counts++;
                                }
                            }
                        }
                    }

                }
            }
        }
    }
}