﻿using System.Xml.Linq;
using GalaSoft.MvvmLight;
using MobilyWP8.Common;

namespace MobilyWP8.VOs
{
    public class BillPersonalInfoVO : ViewModelBase
    {
        private string name;
        private const string NamePropertyName = "Name";
        public string Name
        {
            get { return name; }
            set { Set(NamePropertyName, ref name, value); }
        }
        
        private string billNumber;
        private const string BillNumberPropertyName = "Name";
        public string BillNumber
        {
            get { return billNumber; }
            set { Set(BillNumberPropertyName, ref billNumber, value); }
        }

        private string pOBox;
        private const string POBoxPropertyName = "POBox";
        public string POBox
        {
            get { return pOBox; }
            set { Set(POBoxPropertyName, ref pOBox, value); }
        }

        private string address;
        private const string AddressPropertyName = "Address";
        public string Address {
            get { return address; }
            set { Set(AddressPropertyName, ref address, value); }
        }

        private string city;
        private const string CityPropertyName = "City";
        public string City {
            get { return city; }
            set { Set(CityPropertyName, ref city, value); }
        }

        private string country;
        private const string CountryPropertyName = "Country";
        public string Country
        {
            get { return country; }
            set { Set(CountryPropertyName, ref country, value); }
        }


        public void ParseXmlBillPersonalInfo(string msg)
        {
            msg = msg.Replace("---!>", "-->");
            foreach (
                XElement xelement1 in XElement.Parse(Constants.login_root + msg + Constants.login_rootEnd).Descendants()
                )
            {
                if (xelement1.Name == (XName)Constants.login_MOBILY_IPHONE_REPLY)
                {
                    foreach (XElement xelement2 in xelement1.Descendants())
                    {
                        if (xelement2.Name == (XName)Constants.balancebill_BILL_PERSONAL_INFO)
                        {
                            foreach (XElement xelement3 in xelement2.Descendants())
                            {
                                if (xelement3.Name == (XName)Constants.balancebill_Name)
                                    this.Name = xelement3.Value;
                                else if (xelement3.Name == (XName)Constants.balancebill_BillNumber)
                                    this.BillNumber = xelement3.Value;
                                else if (xelement3.Name == (XName)Constants.balancebill_POBox)
                                    this.POBox = xelement3.Value;
                                else if (xelement3.Name == (XName)Constants.balancebill_Address)
                                    this.Address = xelement3.Value;
                                else if (xelement3.Name == (XName)Constants.balancebill_City)
                                    this.City = xelement3.Value;
                                else if (xelement3.Name == (XName)Constants.balancebill_Country)
                                    this.Country = xelement3.Value;
                            }
                        }
                    }
                }
            }
        }
    }
}