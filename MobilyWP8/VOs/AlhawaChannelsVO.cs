﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.IO.IsolatedStorage;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using GalaSoft.MvvmLight;
using MobilyWP8.Common;
using Windows.ApplicationModel;

namespace MobilyWP8.VOs
{
    public enum MessageTypeEnum
    {
        BillInfoMessage,
        BillPersonalInfoMessage,
        BillSummaryMessage,
        BalanceMessage,
        NeqatyMessage,
        SupplementaryServiceMessage,
        LatestNewsMessage,
        PromotionsMessage,
        BillMethod
    }

    public class ContentChannel
    {
        #region"Variables"
        public List<ContentData> ContentChannelEn { get; set; }
        public List<ContentData> ContentChannelAr { get; set; }
        #endregion
        #region"Sub Class"
        public class ContentData
        {
            #region"Variables"
            public string ContentId { get; set; }
            public string CategoryName { get; set; }
            public string ContentType { get; set; }
            public string Link { get; set; }
            #endregion

            public class Content
            {
                public string ProviderId { get; set; }
                public string ChannelCode { get; set; }
                public string ContentId { get; set; }
                public string LanguageId { get; set; }
                public string Type { get; set; }
                public string Body { get; set; }
                public string CategoryName { get; set; }
                public string UpdateDate { get; set; }
            }
        }
        #endregion
    }

    public class AlhawaChannelsVO : ViewModelBase
    {
        private IEnumerable<ContentChannel.ContentData> _news3alhawa;
        private const string News3alhawaPropertyName = "News3alhawa";
        public IEnumerable<ContentChannel.ContentData> News3alhawa
        {
            get { return _news3alhawa; }
            set { Set(News3alhawaPropertyName, ref _news3alhawa, value); }
        }

        private IEnumerable<ContentChannel.ContentData> _sports3alhawa;
        private const string Sports3alhawaPropertyName = "Sports3alhawa";
        public IEnumerable<ContentChannel.ContentData> Sports3alhawa
        {
            get { return _sports3alhawa; }
            set { Set(Sports3alhawaPropertyName,ref _sports3alhawa, value); }
        }

        //public async Task<ContentChannel.ContentData.Content> GetContentChannelData(string sUrl)
        //{
        //    var content = new ContentChannel.ContentData.Content();

        //    var channelAddress = new Uri(sUrl, UriKind.RelativeOrAbsolute);

        //    var response = await _client.GetAsync(channelAddress);
        //    if (response.IsSuccessStatusCode)
        //    {

        //        byte[] responseBytes = await response.Content.ReadAsByteArrayAsync();
        //        var msg = new UTF8Encoding().GetString(responseBytes, 0, responseBytes.Length);

        //        var xDoc = XElement.Parse(msg);
        //        content.UpdateDate = xDoc.FirstAttribute.Value;

        //        foreach (XElement xelement2 in xDoc.Descendants())
        //        {

        //            if (xelement2.Name == (XName)Constants.AlhawaServices_ContentChannel_Type)
        //            {
        //                content.Type = xelement2.Value;
        //            }
        //            else if (xelement2.Name == Constants.AlhawaServices_ContentChannel_Channel_Code)
        //            {
        //                content.ChannelCode = xelement2.Value;
        //            }
        //            else if (xelement2.Name == Constants.AlhawaServices_ContentChannel_Provider_Id)
        //            {
        //                content.ProviderId = xelement2.Value;
        //            }
        //            else if (xelement2.Name == Constants.AlhawaServices_ContentChannel_Language_Id)
        //            {
        //                content.LanguageId = xelement2.Value;
        //            }
        //            else if (xelement2.Name == Constants.AlhawaServices_ContentChannel_body)
        //            {
        //                content.Body = xelement2.Value;
        //            }
        //        }
        //    }
        //    return content;
        //}
    }
}