﻿using System.Globalization;
using System.Xml.Linq;
using GalaSoft.MvvmLight;
using MobilyWP8.Common;


namespace MobilyWP8.VOs
{
    public class BalanceVO : ViewModelBase
    {
        private string lineNumber;
        private const string LineNumberPropertyName = "LineNumber";
        public string LineNumber
        {
            get { return lineNumber; }
            set { Set(LineNumberPropertyName, ref lineNumber, value); }
        }

        private string expirationDate;
        private const string ExpirationDatePropertyName = "ExpirationDate";
        public string ExpirationDate
        {
            get { return expirationDate; }
            set { Set(ExpirationDatePropertyName, ref expirationDate, value); }
        }
        private string balance;
        private const string BalanceDatePropertyName = "Balance";
        public string Balance
        {
            get { return balance; }
            set { Set(BalanceDatePropertyName,ref balance, value); }
        }

        private string unbilledAmount;
        private const string UnbilledAmountPropertyName = "UnbilledAmount";
        public string UnbilledAmount
        {
            get { return unbilledAmount; }
            set { Set(UnbilledAmountPropertyName, ref unbilledAmount, value); }
        }

        private string dueAmount;
        private const string DueAmountPropertyName = "DueAmount";
        public string DueAmount
        {
            get { return dueAmount; }
            set { Set(DueAmountPropertyName, ref dueAmount, value); }
        }
        private string freeMinutes;
        private const string FreeMinutesPropertyName = "FreeMinutes";
        public string FreeMinutes
        {
            get { return freeMinutes; }
            set { Set(FreeMinutesPropertyName, ref freeMinutes, value); }
        }

        private string freeOnNetMinutes;
        private const string FreeOnNetMinutesPropertyName = "FreeMinutes";
        public string FreeOnNetMinutes
        {
            get { return freeOnNetMinutes; }
            set { Set(FreeOnNetMinutesPropertyName, ref freeOnNetMinutes, value); }
        }


        private string freeSMS;
        private const string FreeSMSPropertyName = "FreeSMS";
        public string FreeSMS
        {
            get { return freeSMS; }
            set { Set(FreeSMSPropertyName,ref freeSMS, value); }
        }
        private string freeOnNetSMS;
        private const string FreeOnNetSMSPropertyName = "FreeOnNetSMS";
        public string FreeOnNetSMS
        {
            get { return freeOnNetSMS; }
            set { Set(FreeOnNetSMSPropertyName, ref freeOnNetSMS, value); }
        }

        private string freeMMS;
        private const string FreeMMSPropertyName = "FreeMMS";
        public string FreeMMS
        {
            get { return freeMMS; }
            set { Set(FreeMMSPropertyName, ref freeMMS, value); }
        }


        private string freeOnNetMMS;
        private const string FreeOnNetMMSPropertyName = "FreeOnNetMMS";
        public string FreeOnNetMMS
        {
            get { return freeOnNetMMS; }
            set { Set(FreeOnNetMMSPropertyName, ref freeOnNetMMS, value); }
        }
        private string freeGPRS;
        private const string FreeGPRSPropertyName = "FreeGPRS";
        public string FreeGPRS
        {
            get { return freeGPRS; }
            set { Set(FreeGPRSPropertyName, ref freeGPRS, value); }
        }

        private string nationalFavoriteNumber;
        private const string NationalFavoriteNumberPropertyName = "NationalFavoriteNumber";
        public string NationalFavoriteNumber
        {
            get { return nationalFavoriteNumber; }
            set { Set(NationalFavoriteNumberPropertyName, ref nationalFavoriteNumber, value); }
        }
        private string internationalFavoriteNumber;
        private const string InternationalFavoriteNumberPropertyName = "NationalFavoriteNumber";
        public string InternationalFavoriteNumber
        {
            get { return internationalFavoriteNumber; }
            set { Set(InternationalFavoriteNumberPropertyName, ref internationalFavoriteNumber, value); }
        }

        public void ParseXml(string msg)
        { 
            msg = msg.Replace("---!>", "-->");
            foreach (XElement xelement1 in XElement.Parse(Constants.login_root + msg + Constants.login_rootEnd).Descendants())
            {
                if (xelement1.Name == (XName)Constants.login_MOBILY_IPHONE_REPLY)
                {
                    foreach (XElement xelement2 in xelement1.Descendants())
                    {
                        if (xelement2.Name == (XName)Constants.balancebill_BALANCE)
                        {
                            foreach (XElement xelement3 in xelement2.Descendants())
                            {
                                if (xelement3.Name == (XName)Constants.balancebill_LINENUMBER)
                                    this.LineNumber = xelement3.Value;
                                else if (xelement3.Name == (XName)Constants.balancebill_EXPIRATION_DATE)
                                    this.ExpirationDate = xelement3.Value;
                                else if (xelement3.Name == (XName)Constants.balancebill_BALANCE)
                                    this.Balance = xelement3.Value;
                                else if (xelement3.Name == (XName)Constants.balancebill_UnbilledAmount)
                                    this.UnbilledAmount = xelement3.Value;
                                else if (xelement3.Name == (XName)Constants.balancebill_DueAmount)
                                    this.DueAmount = xelement3.Value;
                                else if (xelement3.Name == (XName)Constants.balancebill_FreeMinutes)
                                {
                                    if (xelement3.Value != null && xelement3.Value != "")
                                        this.FreeMinutes = xelement3.Value;
                                }
                                else if (xelement3.Name == (XName)Constants.balancebill_FreeOnNetMinutes)
                                {
                                    if (xelement3.Value != null && xelement3.Value != "")
                                        this.FreeOnNetMinutes = xelement3.Value;
                                }
                                else if (xelement3.Name == (XName)Constants.balancebill_FreeSMS)
                                {
                                    if (xelement3.Value != null && xelement3.Value != "")
                                        this.FreeSMS = xelement3.Value;
                                }
                                else if (xelement3.Name == (XName)Constants.balancebill_FreeOnNetSMS)
                                {
                                    if (xelement3.Value != null && xelement3.Value != "")
                                        this.FreeOnNetSMS = xelement3.Value;
                                }
                                else if (xelement3.Name == (XName)Constants.balancebill_FreeMMS)
                                {
                                    if (xelement3.Value != null && xelement3.Value != "")
                                        this.FreeMMS = xelement3.Value;
                                }
                                else if (xelement3.Name == (XName)Constants.balancebill_FreeOnNetMMS)
                                {
                                    if (xelement3.Value != null && xelement3.Value != "")
                                        this.FreeOnNetMMS = xelement3.Value;
                                }
                                else if (xelement3.Name == (XName)Constants.balancebill_FreeGPRS)
                                {
                                    if (xelement3.Value != null && xelement3.Value != "")
                                        this.FreeGPRS = xelement3.Value;
                                }
                                else if (xelement3.Name == (XName)Constants.balancebill_NationalFavoriteNumber)
                                    this.NationalFavoriteNumber = xelement3.Value;
                                else if (xelement3.Name == (XName)Constants.balancebill_InternationalFavoriteNumber)
                                    this.InternationalFavoriteNumber = xelement3.Value;
                            }
                        }
                    }
                }
            }
        }
    }
}