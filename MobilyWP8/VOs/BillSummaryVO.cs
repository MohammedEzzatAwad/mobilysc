﻿using System.Globalization;
using System.Xml.Linq;
using GalaSoft.MvvmLight;
using MobilyWP8.Common;

namespace MobilyWP8.VOs
{
    public class BillSummaryVO : ViewModelBase
    {
        private string previousAmount;
        private const string PreviousAmountPropertyName = "PreviousAmount";
        public string PreviousAmount
        {
            get { return previousAmount; }
            set { Set(PreviousAmountPropertyName, ref previousAmount, value); }
        }


        private string monthlyFee;
        private const string MonthlyFeePropertyName = "PreviousAmount";
        public string MonthlyFee
        {
            get { return monthlyFee; }
            set { Set(MonthlyFeePropertyName, ref monthlyFee, value); }
        }

        private string additionalFee;
        private const string AdditionalFeePropertyName = "PreviousAmount";
        public string AdditionalFee
        {
            get { return additionalFee; }
            set { Set(AdditionalFeePropertyName, ref additionalFee, value); }
        }
        private string usageAmount;
        private const string UsageAmountFeePropertyName = "UsageAmount";
        public string UsageAmount
        {
            get { return usageAmount; }
            set { Set(UsageAmountFeePropertyName, ref usageAmount, value); }
        }
        private string discount;
        private const string DiscountPropertyName = "Discount";
        public string Discount
        {
            get { return discount; }
            set { Set(DiscountPropertyName, ref discount, value); }
        }
        private string paidAmount;
        private const string PaidAmountPropertyName = "PaidAmount";
        public string PaidAmount
        {
            get { return paidAmount; }
            set { Set(PaidAmountPropertyName, ref paidAmount, value); }
        }

        private string amountDue;
        private const string AmountDuePropertyName = "AmountDue";
        public string AmountDue
        {
            get { return amountDue; }
            set { Set(AmountDuePropertyName, ref amountDue, value); }
        }


        public void ParseXmlBillSummary(string msg)
        {
            msg = msg.Replace("---!>", "-->");
            foreach (
                XElement xelement1 in XElement.Parse(Constants.login_root + msg + Constants.login_rootEnd).Descendants()
                )
            {
                if (xelement1.Name == (XName) Constants.login_MOBILY_IPHONE_REPLY)
                {
                    foreach (XElement xelement2 in xelement1.Descendants())
                    {
                        if (xelement2.Name == (XName) Constants.balancebill_BILL_SUMMARY)
                        {
                            foreach (XElement xelement3 in xelement2.Descendants())
                            {
                                CultureInfo invariantCulture = CultureInfo.InvariantCulture;
                                if (xelement3.Name == (XName) Constants.balancebill_Previous_Amount)
                                {
                                    if (xelement3.Value != null && xelement3.Value != "")
                                        this.PreviousAmount = xelement3.Value;
                                }
                                else if (xelement3.Name == (XName) Constants.balancebill_MonthlyFee)
                                {
                                    if (xelement3.Value != null && xelement3.Value != "")
                                        this.MonthlyFee = xelement3.Value;
                                }
                                else if (xelement3.Name == (XName) Constants.balancebill_AdditionalFee)
                                {
                                    if (xelement3.Value != null && xelement3.Value != "")
                                        this.AdditionalFee = xelement3.Value;
                                }
                                else if (xelement3.Name == (XName) Constants.balancebill_UsageAmount)
                                {
                                    if (xelement3.Value != null && xelement3.Value != "")
                                        //TODO: parse signed value
                                        this.UsageAmount = xelement3.Value;
                                }
                                else if (xelement3.Name == (XName) Constants.balancebill_Discount)
                                {
                                    if (xelement3.Value != null && xelement3.Value != "")
                                        //TODO: parse signed value
                                        this.Discount = xelement3.Value;
                                }
                                else if (xelement3.Name == (XName) Constants.balancebill_PaidAmount)
                                {
                                    if (xelement3.Value != null && xelement3.Value != "")
                                        //TODO: parse signed value
                                        this.PaidAmount = xelement3.Value;
                                }
                                else if (xelement3.Name == (XName) Constants.balancebill_AmountDue &&
                                         (xelement3.Value != null && xelement3.Value != ""))
                                    this.AmountDue = xelement3.Value;
                            }
                        }
                    }
                }
            }
        }
    }
}