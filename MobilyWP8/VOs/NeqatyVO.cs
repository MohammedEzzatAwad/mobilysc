﻿using System.Globalization;
using System.Xml.Linq;
using GalaSoft.MvvmLight;
using MobilyWP8.Common;

namespace MobilyWP8.VOs
{
    public class NeqatyVO : ViewModelBase
    {
        private string currentBalance;
        private const string CurrentBalancePropertyName = "CurrentBalance";
        public string CurrentBalance
        {
            get { return currentBalance; }
            set { Set(CurrentBalancePropertyName, ref currentBalance, value); }
        }
        private string earnedPoints;
        private const string EarnedPointsPropertyName = "EarnedPoints";
        public string EarnedPoints
        {
            get { return earnedPoints; }
            set { Set(EarnedPointsPropertyName, ref earnedPoints, value); }
        }

        private string expirySchedulePoints;
        private const string ExpirySchedulePointsPropertyName = "EarnedPoints";
        public string ExpirySchedulePoints
        {
            get { return expirySchedulePoints; }
            set { Set(ExpirySchedulePointsPropertyName, ref expirySchedulePoints, value); }
        }
        
        private string lostPoints;
        private const string LostPointsPointsPropertyName = "LostPoints";
        public string LostPoints
        {
            get { return lostPoints; }
            set { Set(LostPointsPointsPropertyName, ref lostPoints, value); }
        }
        
        private string redeemedPoints;
        private const string RedeemedPointsPropertyName = "RedeemedPoints";
        public string RedeemedPoints
        {
            get { return redeemedPoints; }
            set { Set(RedeemedPointsPropertyName, ref redeemedPoints, value); }
        } 
        
        public void ParseXml(string msg)
        {
            CultureInfo invariantCulture = CultureInfo.InvariantCulture;
            string str = string.Empty;
            msg = msg.Replace("---!>", "-->");
            foreach (XElement xelement1 in XElement.Parse(Constants.login_root + msg + Constants.login_rootEnd).Descendants())
            {
                if (xelement1.Name == (XName)Constants.login_MOBILY_IPHONE_REPLY)
                {
                    foreach (XElement xelement2 in xelement1.Descendants())
                    {
                        //if (xelement2.Name == (XName)Constants.neqaty_LOYALTY_INQUIRY_ITEMS)
                        //{


                        if (xelement2.Name == (XName)Constants.neqaty_CurrentBalance)
                        {
                            if (xelement2.Value != null && xelement2.Value != "")
                                this.CurrentBalance = xelement2.Value;

                        }
                        else if (xelement2.Name == (XName)Constants.neqaty_EarnedPoints)
                        {
                            if (xelement2.Value != null && xelement2.Value != "")
                                this.EarnedPoints = xelement2.Value;

                        }
                        else if (xelement2.Name == (XName)Constants.neqaty_ExpirySchedulerPoints)
                        {
                            if (xelement2.Value != null && xelement2.Value != "")
                                this.ExpirySchedulePoints = xelement2.Value;
                        }
                        else if (xelement2.Name == (XName)Constants.neqaty_LostPoints)
                        {
                            if (xelement2.Value != null && xelement2.Value != "")
                                this.LostPoints = xelement2.Value;
                        }
                        else if (xelement2.Name == (XName)Constants.neqaty_RedeemedPoints)
                        {
                            if (xelement2.Value != null && xelement2.Value != "")
                                this.RedeemedPoints = xelement2.Value;
                        }
                    }
                    //}
                    //}
                }
            }
        }
    }
}