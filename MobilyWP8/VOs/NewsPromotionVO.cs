﻿using System.Windows.Media.Imaging;
using GalaSoft.MvvmLight;
using MobilyWP8.Common;


namespace MobilyWP8.VOs
{
    public class NewsPromotionVO :ViewModelBase
    {
        public string Id { get; set; }
        public string ShortDescEn { get; set; }
        public string ShortDescAr { get; set; }
        public string DescEn { get; set; }
        public string DescAr { get; set; }
        public BitmapImage Image { get; set; }
        public string ImageFormat { get; set; }

        public string Type { get; set; }
    }
}