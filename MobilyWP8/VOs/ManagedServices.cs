﻿using System.Collections.Generic;

namespace MobilyWP8.VOs
{
    public class ManagedServices
    {
        public string ServiceName { get; set; }
        public string ServiceNameForDisplay { get; set; }
        public string ServiceNameEng { get; set; }
        public string ServiceNameAr { get; set; }
        public bool Status { get; set; }
        public string ServiceTypeOperation { get; set; }
        public string SubscriptionFee { get; set; }
        public string MonthlyFee { get; set; }

        public List<ServiceType> ListOfServiceType { get; set; }

        public class ServiceType
        {
            public string ServiceTypeName { get; set; }
            public List<Subtype> ListOfSubType  { get; set; }

            public class Subtype
            {
                public string SubTypeName { get; set; }
                public string SelectedSubType { get; set; }
            }
        }
    }
}