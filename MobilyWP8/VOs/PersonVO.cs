﻿using GalaSoft.MvvmLight;
using MobilyWP8.Common;

namespace MobilyWP8.VOs
{
    public class PersonVO : ViewModelBase
    {
        public PersonVO()
        {
            BillInfo = new BillInfoVO();
            BillPersonalInfo = new BillPersonalInfoVO();
            BillSummary = new BillSummaryVO();
            Balance = new BalanceVO();
            Neqaty = new NeqatyVO();
            NotificationAndServices = new NotificationAndServicesVO();
            BillMethod = new BillMethodVO();
            SupplementaryServices = new SupplementaryServicesVO();
            AlhawaChannels = new AlhawaChannelsVO();
        }

        private string hashCode;
        private const string HashCodePropertyName = "HashCode";
        public string HashCode
        {
            get { return hashCode; }
            set { Set(HashCodePropertyName, ref hashCode, value); }
        }


        private BillInfoVO _billInfoVO;
        private const string BillInfoPropertyName = "BillInfo";
        public BillInfoVO BillInfo
        {
            get{return _billInfoVO;}
            set { Set(BillInfoPropertyName, ref _billInfoVO, value); }
        }

        private BillPersonalInfoVO _billPersonalInfoVO;
        private const string BillPersonalInfoPropertyName = "BillPersonalInfo";
        public BillPersonalInfoVO BillPersonalInfo
        {
            get { return _billPersonalInfoVO; }
            set { Set(BillPersonalInfoPropertyName, ref _billPersonalInfoVO, value); }
        }

        private BillSummaryVO _billSummaryVO;
        private const string BillSummaryPropertyName = "BillSummary";
        public BillSummaryVO BillSummary
        {
            get { return _billSummaryVO; }
            set { Set(BillSummaryPropertyName, ref _billSummaryVO, value); }
        }

        private BalanceVO _balanceVO;
        private const string BalancePropertyName = "Balance";
        public BalanceVO Balance
        {
            get { return _balanceVO; }
            set { Set(BalancePropertyName, ref _balanceVO, value); }
        }

        private NeqatyVO _neqaty;
        private const string NeqatyPropertyName = "Neqaty";
        public NeqatyVO Neqaty
        {
            get { return _neqaty; }
            set { Set(NeqatyPropertyName, ref _neqaty, value); }
        }

        private NotificationAndServicesVO _notificationAndServices;
        private const string NotificationAndServicesPropertyName = "NotificationAndServices";
        public NotificationAndServicesVO NotificationAndServices
        {
            get { return _notificationAndServices; }
            set { Set(NotificationAndServicesPropertyName, ref _notificationAndServices, value); }
        }

        private BillMethodVO _billMethod;
        private const string BillMethodPropertyName = "BillMethod";
        public  BillMethodVO BillMethod
        {
            get { return _billMethod; }
            set { Set(BillMethodPropertyName, ref _billMethod, value); }
        }

        private SupplementaryServicesVO _supplementaryServices;
        private const string SupplementaryServicesPropertyName = "SupplementaryServices";
        public SupplementaryServicesVO SupplementaryServices
        {
            get { return _supplementaryServices; }
            set { Set(SupplementaryServicesPropertyName, ref _supplementaryServices, value); }
        }

        private AlhawaChannelsVO _alhawaChannels;
        private const string AlhawaChannelsPropertyName = "AlhawaChannels";
        public AlhawaChannelsVO AlhawaChannels
        {
            get { return _alhawaChannels; }
            set { Set(AlhawaChannelsPropertyName, ref _alhawaChannels, value); }
        }
    }
}