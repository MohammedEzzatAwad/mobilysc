﻿using System.Xml.Linq;
using GalaSoft.MvvmLight;
using MobilyWP8.Common;


namespace MobilyWP8.VOs
{
    public class BillInfoVO : ViewModelBase
    {
        private string startDate;
        private const string StartDatePropertyName = "StartDate";
        public string StartDate
        {
            get { return startDate; }
            set { Set(StartDatePropertyName, ref startDate, value); }
        }
        private string endDate;
        private const string EndDatePropertyName = "EndDate";
        public string EndDate
        {
            get { return endDate; }
            set { Set(EndDatePropertyName, ref endDate, value); }
        }

        private string tarrifPlan;
        private const string TarrifPlanPropertyName = "TarrifPlan";
        public string TarrifPlan
        {
            get { return tarrifPlan; }
            set { Set(TarrifPlanPropertyName, ref tarrifPlan, value); }
        }

        private string lineNumber;
        private const string LineNumberPropertyName = "LineNumber";
        public string LineNumber
        {
            get { return lineNumber; }
            set { Set(LineNumberPropertyName, ref lineNumber, value); }
        }

        private string dueDate;
        private const string DueDatePropertyName = "DueDate";
        public string DueDate
        {
            get { return dueDate; }
            set { Set(DueDatePropertyName, ref dueDate, value); }
        }

        public void ParseXmlBillInfo(string msg)
        {
            msg = msg.Replace("---!>", "-->");
            foreach (
                XElement xelement1 in XElement.Parse(Constants.login_root + msg + Constants.login_rootEnd).Descendants()
                )
            {
                if (xelement1.Name == (XName)Constants.login_MOBILY_IPHONE_REPLY)
                {
                    foreach (XElement xelement2 in xelement1.Descendants())
                    {
                        if (xelement2.Name == (XName)Constants.balancebill_BILL_INFO)
                        {
                            foreach (XElement xelement3 in xelement2.Descendants())
                            {
                                if (xelement3.Name == (XName)Constants.balancebill_StartDate)
                                    this.StartDate = xelement3.Value;
                                else if (xelement3.Name == (XName)Constants.balancebill_EndDate)
                                    this.EndDate = xelement3.Value;
                                else if (xelement3.Name == (XName)Constants.balancebill_Tarrif_Plan)
                                    this.TarrifPlan = xelement3.Value;
                                else if (xelement3.Name == (XName)Constants.balancebill_LINENUMBER)
                                    this.LineNumber = xelement3.Value;
                                else if (xelement3.Name == (XName)Constants.balancebill_Due_Date)
                                    this.DueDate = xelement3.Value;
                            }
                        }
                    }
                }
            }
        }
    }
}