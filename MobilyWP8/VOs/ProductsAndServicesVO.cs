﻿using GalaSoft.MvvmLight;
using MobilyWP8.Common;

namespace MobilyWP8.VOs
{
    public class ProductsAndServicesVO : ViewModelBase
    {
        protected string m_refName;
        protected string m_refTitle;
        protected string m_refLogo;
        protected string m_refLink;
        protected string m_refCategory;
        protected string m_refDescription;

        protected int m_refIndex;

        private const string NamePropertyName = "Name";
        public string Name
        {
            get { return this.m_refName; }
            set { Set(NamePropertyName, ref m_refName, value); }
        }

        private const string TitlePropertyName = "Title";
        public string Title
        {
            get { return this.m_refTitle; }
            set { Set(TitlePropertyName, ref m_refTitle, value); }
        }
        private const string LinkPropertyName = "Link";
        public string Link
        {
            get { return this.m_refLink; }
            set { Set(LinkPropertyName, ref m_refLink, value); }

        }
        private const string LogoPropertyName = "Logo";
        public string Logo
        {
            get { return this.m_refLogo; }
            set { Set(LogoPropertyName, ref m_refLogo, value); }
        }

        private const string CategoryPropertyName = "Category";
        public string Category
        {
            get { return this.m_refCategory; }
            set { Set(CategoryPropertyName, ref m_refCategory, value); }
        }
        private const string DescriptionPropertyName = "Description";
        public string Description
        {
            get { return this.m_refDescription; }
            set { Set(DescriptionPropertyName, ref m_refDescription, value); }
        }
        private const string IndexPropertyName = "Index";
        public int Index
        {
            get { return this.m_refIndex; }
            set { Set(IndexPropertyName, ref m_refIndex, value); }

        }

         
    }
}