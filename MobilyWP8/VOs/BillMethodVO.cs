﻿using System.Xml.Linq;
using GalaSoft.MvvmLight;
using MobilyWP8.Common;

namespace MobilyWP8.VOs
{
    public enum BillMethodStatus
    {
        POBOX,
        EMAIL
    }

    public class BillMethodVO : ViewModelBase
    {
        private string _email;
        private const string EmailPropertyName = "Email";
        public string Email
        {
            get { return _email; }
            set { Set(EmailPropertyName, ref _email, value); }
        }

        private BillMethodStatus _billMethodStatus;
        private const string BillMethodStatusPropertyName = "BillMethodStatus";
        public BillMethodStatus BillMethodStatus
        {
            get { return _billMethodStatus; }
            set
            {
                Set(BillMethodStatusPropertyName, ref _billMethodStatus,
                    value);
            }
        }

        public void ParseBillMethodXml(string msg)
        {
            msg = msg.Replace("---!>", "-->");
            foreach (
                XElement xelement1 in
                    XElement.Parse(Constants.login_root + msg + Constants.login_rootEnd).Descendants()
                )
            {
                if (xelement1.Name == (XName)Constants.login_MOBILY_IPHONE_REPLY)
                {
                    foreach (XElement xelement2 in xelement1.Descendants())
                    {
                        if (xelement2.Name == Constants.BILL_METHOD_InvoiceMechanism)
                        {
                            if (xelement2.Value == "POBOX")
                            {
                                BillMethodStatus = BillMethodStatus.POBOX;
                            }
                            else if (xelement2.Value == "Email")
                            {
                                BillMethodStatus = BillMethodStatus.EMAIL;
                            }
                        }
                        else if (xelement2.Name == Constants.BILL_METHOD_Email)
                        {
                            Email = xelement2.Value;
                        }
                    }
                }
            }
        }
    }
}