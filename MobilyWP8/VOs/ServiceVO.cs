﻿namespace MobilyWP8.VOs
{
    public class ServiceVO
    {
        public string ServiceName { get; set; }
        public string Status { get; set; }
        public string SubscriptionFee { get; set; }
        public string MonthlyFee { get; set; }
        public string ServiceType { get; set; }
    }
}