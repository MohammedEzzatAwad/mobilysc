﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.IO.IsolatedStorage;
using System.Linq;
using System.Threading.Tasks;
using System.Xml.Linq;
using GalaSoft.MvvmLight;
using MobilyWP8.Common;
using Windows.ApplicationModel;
using Windows.Storage;

namespace MobilyWP8.VOs
{
    public class SupplementaryServicesVO : ViewModelBase
    {

        public SupplementaryServicesVO()
        {
            Services = new ObservableCollection<ServiceVO>();
            CurrentUserManagedServices = new List<ManagedServices>();
        }

        protected ObservableCollection<ServiceVO> _servicesColl = (ObservableCollection<ServiceVO>)null;

        private const string ServicesPropertyName = "Services";
        private ObservableCollection<ServiceVO> Services
        {
            get
            {
                return this._servicesColl;
            }
            set
            { this.Set(ServicesPropertyName, ref _servicesColl, value); }
        }

        private List<ManagedServices> _currentUserManagedServices;
        private const string CurrentUserManagedServicesPropertyName = "CurrentUserManagedServices";
        public List<ManagedServices> CurrentUserManagedServices
        {
            get { return _currentUserManagedServices; }
            set { Set(CurrentUserManagedServicesPropertyName, ref _currentUserManagedServices, value); }
        }

        private List<ManagedServices> _activeUserManagedServices;
        public List<ManagedServices> ActiveUserManagedServices
        {
            get { return _activeUserManagedServices; }
            set { _activeUserManagedServices = value; }
        }

        private async void GetSupplemantaryServices()
        {
            List<ManagedServices> lstManagedServicesdb = await GetManagedService();

            try
            {
                foreach (ManagedServices ManagedserviceItem in lstManagedServicesdb)
                {
                    ServiceVO objCurrentService = Services.ToList().Where(ServiceItem => ServiceItem.ServiceName == ManagedserviceItem.ServiceName).SingleOrDefault();

                    if (objCurrentService != null)
                    {
                        ManagedserviceItem.Status = GetBooleanValue(objCurrentService.Status);
                        //ManagedserviceItem.ServiceTypeOption = objCurrentService.ServiceType;

                        ManagedserviceItem.SubscriptionFee = objCurrentService.SubscriptionFee;
                        ManagedserviceItem.MonthlyFee = objCurrentService.MonthlyFee;

                        string sSubType = objCurrentService.ServiceType;

                        foreach (ManagedServices.ServiceType ServiceType in ManagedserviceItem.ListOfServiceType)
                        {
                            ManagedServices.ServiceType.Subtype SubTypeIyem = ServiceType.ListOfSubType.Where(SubItem => SubItem.SubTypeName == sSubType).SingleOrDefault();

                            if (SubTypeIyem != null)
                            { SubTypeIyem.SelectedSubType = "true"; }
                        }

                    }
                }
            }
            catch
            {
                throw;
            }

            CurrentUserManagedServices = lstManagedServicesdb;


            ActiveUserManagedServices = CurrentUserManagedServices.Where(s => s.Status == true).ToList();
        }

        private async Task<List<ManagedServices>> GetManagedService()
        {
            List<ManagedServices> ListOfManagedSevices = new List<ManagedServices>();

            try 
            {
                //var storageFolder = await Package.Current.InstalledLocation.GetFolderAsync("LocalData");
                //var xmlFile = await storageFolder.GetFileAsync("ManageServices.xml");
                
                //XmlDocument xmlDoc = await XmlDocument.LoadFromFileAsync(xmlFile);
                //XDocument xDoc = XDocument.Parse(xmlDoc.GetXml());

                XDocument xDoc = XDocument.Load("Assets\\LocalData\\ManageServices.xml");
                ListOfManagedSevices = (from ServicesItem in xDoc.Elements("MANAGESERVICES").Elements("MANAGESERVICE")
                                        select new ManagedServices
                                        {

                                            ServiceName = ServicesItem.Elements("ServiceName").Any() ? ServicesItem.Element("ServiceName").Value.ToString() : string.Empty,
                                            ServiceNameAr = ServicesItem.Elements("ServiceNameAr").Any() ? ServicesItem.Element("ServiceNameAr").Value.ToString() : string.Empty,
                                            ServiceNameEng = ServicesItem.Elements("ServiceNameEng").Any() ? ServicesItem.Element("ServiceNameEng").Value.ToString() : string.Empty,
                                            Status = ServicesItem.Elements("Status").Any() ? GetBooleanValue(ServicesItem.Element("Status").Value.ToString()) : false,
                                            MonthlyFee = ServicesItem.Elements("MonthlyFee").Any() ? ServicesItem.Element("MonthlyFee").Value.ToString() : string.Empty,
                                            SubscriptionFee = ServicesItem.Elements("SubscriptionFee").Any() ? ServicesItem.Element("SubscriptionFee").Value.ToString() : string.Empty,

                                            ListOfServiceType = (from ServiceTypeItem in ServicesItem.Elements("ServiceTypes").Elements("ServiceType")
                                                                 select new ManagedServices.ServiceType
                                                                 {
                                                                     ServiceTypeName = ServiceTypeItem.Elements("ServiceTypeName").Any() ? ServiceTypeItem.Element("ServiceTypeName").Value.ToString() : string.Empty,
                                                                     ListOfSubType = (from SubTypeItem in ServiceTypeItem.Elements("SubTypes").Elements("SubType")
                                                                                      select new ManagedServices.ServiceType.Subtype
                                                                                      {
                                                                                          SubTypeName = SubTypeItem.Elements("SubTypeName").Any() ? SubTypeItem.Element("SubTypeName").Value.ToString() : string.Empty,
                                                                                          SelectedSubType = string.Empty
                                                                                      }).ToList<ManagedServices.ServiceType.Subtype>(),
                                                                 }).ToList<ManagedServices.ServiceType>(),


                                        }).ToList<ManagedServices>();

                return ListOfManagedSevices;
            }
            catch { throw; }
        }
        private bool GetBooleanValue(string value)
        {
            if (value == "1")
                return true;
            else
                return false;
        }

        public void ParseXml(string msg)
        {
            msg = msg.Replace("---!>", "-->");
            foreach (XElement xelement1 in XElement.Parse(Constants.login_root + msg + Constants.login_rootEnd).Descendants())
            {
                if (xelement1.Name == (XName)Constants.login_MOBILY_IPHONE_REPLY)
                {
                    foreach (XElement xelement2 in xelement1.Descendants())
                    {
                        if (xelement2.Name == (XName)Constants.SUPPLEMENTARY_SERVICES_INQUIRY)
                        {
                            foreach (XElement xelement3 in xelement2.Descendants())
                            {
                                if (xelement3.Name == (XName)Constants.ListOfSupplementaryService)
                                {
                                    Services = new ObservableCollection<ServiceVO>();

                                    foreach (var xelement4 in xelement3.Descendants())
                                    {
                                        if (xelement4.Name == (XName)Constants.SupplementaryService)
                                        {
                                            ServiceVO service = new ServiceVO();

                                            foreach (var xelement5 in xelement4.Descendants())
                                            {
                                                if (xelement5.Name == Constants.SupplementaryService_ServiceName)
                                                {
                                                    service.ServiceName = xelement5.Value;
                                                }
                                                else if (xelement5.Name == Constants.SupplementaryService_Status)
                                                {
                                                    service.Status = xelement5.Value;
                                                }
                                                else if (xelement5.Name == Constants.SupplementaryService_SubscriptionFee)
                                                {
                                                    service.SubscriptionFee = xelement5.Value;
                                                }
                                                else if (xelement5.Name == Constants.SupplementaryService_MonthlyFee)
                                                {
                                                    service.MonthlyFee = xelement5.Value;
                                                }
                                                
                                            }
                                            Services.Add(service);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            
            GetSupplemantaryServices();
        }
    }
}